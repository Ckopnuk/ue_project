// Shoot_Them_Up game from Ckopnuk

using UnrealBuildTool;

public class Shoot_Them_Up : ModuleRules
{
    public Shoot_Them_Up(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "Niagara",
            "GameplayTasks",
            "NavigationSystem",
            "PhysicsCore"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        PublicIncludePaths.AddRange(new string[]
        {
            "Shoot_Them_Up/Public/Player",
            "Shoot_Them_Up/Public/Component",
            "Shoot_Them_Up/Public/Dev",
            "Shoot_Them_Up/Public/Weapon",
            "Shoot_Them_Up/Public/Weapon/Components",
            "Shoot_Them_Up/Public/UI",
            "Shoot_Them_Up/Public/Animations",
            "Shoot_Them_Up/Public/AI",
            "Shoot_Them_Up/Public/AI/Tasks",
            "Shoot_Them_Up/Public/AI/Service",
            "Shoot_Them_Up/Public/AI/EQS",
            "Shoot_Them_Up/Public/AI/Decorators",
            "Shoot_Them_Up/Public/Pickups",
            "Shoot_Them_Up/Public/Menu",
            "Shoot_Them_Up/Public/Menu/UI"
        });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
