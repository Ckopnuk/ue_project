// Shoot_Them_Up game from Ckopnuk


#include "Pickups/MySTU_Health_Pickup.h"
#include "Component/STU_Helth_Component.h"
#include "STU_Utils.h"

DEFINE_LOG_CATEGORY_STATIC(health_pickups_log, All, All);

bool AMySTU_Health_Pickup::give_pickup_to(APawn* player_pawn)
{
    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(player_pawn);
    if (!health_component || health_component->is_dead())
        return false;

    return health_component->try_to_add_health(health_amount);
}