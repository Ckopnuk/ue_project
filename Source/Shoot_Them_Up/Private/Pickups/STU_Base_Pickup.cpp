// Shoot_Them_Up game from Ckopnuk

#include "Pickups/STU_Base_Pickup.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(base_pickups_log, All, All);

ASTU_Base_Pickup::ASTU_Base_Pickup()
{
    PrimaryActorTick.bCanEverTick = true;

    collision_component = CreateDefaultSubobject<USphereComponent>("Sphere_component");
    collision_component->InitSphereRadius(50.0);
    collision_component->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    collision_component->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    SetRootComponent(collision_component);
}

void ASTU_Base_Pickup::BeginPlay()
{
    Super::BeginPlay();
    generate_rotation_yaw();
}

void ASTU_Base_Pickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    const auto pawn = Cast<APawn>(OtherActor);
    if (give_pickup_to(pawn))
    {
        pickup_was_taken();
    }
}

void ASTU_Base_Pickup::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    AddActorLocalRotation(FRotator(0.0f, rotation_yaw, 0.0f));
}

bool ASTU_Base_Pickup::give_pickup_to(APawn* player_pawn)
{
    return false;
}

void ASTU_Base_Pickup::pickup_was_taken()
{
    collision_component->SetCollisionResponseToAllChannels(ECR_Ignore);
    if (GetRootComponent())
    {
        GetRootComponent()->SetVisibility(false, true);
    }

    GetWorldTimerManager().SetTimer(respawn_timer_handle, this, &ASTU_Base_Pickup::respawn,
                                    respawn_time);

    UGameplayStatics::PlaySoundAtLocation(GetWorld(), pickup_taken_sound, GetActorLocation());
}

void ASTU_Base_Pickup::respawn()
{
    collision_component->SetCollisionResponseToAllChannels(ECR_Overlap);
    if (GetRootComponent())
    {
        GetRootComponent()->SetVisibility(true, true);
    }
    generate_rotation_yaw();
}

void ASTU_Base_Pickup::generate_rotation_yaw()
{
    const float direction = FMath::RandBool() ? 1.0f : -1.0f;
    rotation_yaw          = FMath::RandRange(1.0f, 3.0f) * direction;
}

bool ASTU_Base_Pickup::could_be_taken() const
{
    return !GetWorldTimerManager().IsTimerActive(respawn_timer_handle);
}