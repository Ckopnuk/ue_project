// Shoot_Them_Up game from Ckopnuk

#include "Pickups/STU_Ammo_Pickup.h"
#include "Component/STU_Helth_Component.h"
#include "Component/STU_Weapon_Component.h"
#include "STU_Utils.h"

DEFINE_LOG_CATEGORY_STATIC(ammo_pickups_log, All, All);

bool ASTU_Ammo_Pickup::give_pickup_to(APawn* player_pawn)
{
    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(player_pawn);
    if (!health_component || health_component->is_dead())
        return false;

    const auto weapon_component =
        STU_Utils::get_STU_player_component<USTU_Weapon_Component>(player_pawn);
    if (!weapon_component)
        return false;

    
    return weapon_component->try_to_add_ammo(weapon_type, clips_amount);
}