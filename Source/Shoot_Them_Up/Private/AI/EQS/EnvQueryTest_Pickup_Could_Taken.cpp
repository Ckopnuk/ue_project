// Shoot_Them_Up game from Ckopnuk

#include "AI/EQS/EnvQueryTest_Pickup_Could_Taken.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "Pickups/STU_Base_Pickup.h"

UEnvQueryTest_Pickup_Could_Taken::UEnvQueryTest_Pickup_Could_Taken(
    const FObjectInitializer& object_initializer)
    : Super(object_initializer)
{
    Cost          = EEnvTestCost::Low;
    ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
    SetWorkOnFloatValues(false);
}

void UEnvQueryTest_Pickup_Could_Taken::RunTest(FEnvQueryInstance& QueryInstance) const
{
    UObject* DataOwner = QueryInstance.Owner.Get();
    BoolValue.BindData(DataOwner, QueryInstance.QueryID);

    bool wants_be_takeble = BoolValue.GetValue();
    for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
    {
        AActor*    ItemActor    = GetItemActor(QueryInstance, It.GetIndex());
        const auto pickup_actor = Cast<ASTU_Base_Pickup>(ItemActor);
        if (!pickup_actor)
            continue;

        const auto could_be_taken = pickup_actor->could_be_taken();
        It.SetScore(TestPurpose, FilterType, could_be_taken, wants_be_takeble);
    }
}
