// Shoot_Them_Up game from Ckopnuk

#include "AI/EQS/STU_Enemy_Env_Query_Context.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

//    USTU_Enemy_Env_Query_Context::USTU_Enemy_Env_Query_Context();

void USTU_Enemy_Env_Query_Context::ProvideContext(FEnvQueryInstance&    query_instance,
                                                  FEnvQueryContextData& context_data) const
{
    const auto                  query_owner = Cast<AActor>(query_instance.Owner.Get());
    const UBlackboardComponent* blackboard  = UAIBlueprintHelperLibrary::GetBlackboard(query_owner);
    if (!blackboard)
        return;
    const auto context_actor = blackboard->GetValueAsObject(enemy_actor_key_name);
    UEnvQueryItemType_Actor::SetContextHelper(context_data, Cast<AActor>(context_actor));
}