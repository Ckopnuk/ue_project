// Shoot_Them_Up game from Ckopnuk

#include "AI/Decorators/STU_Ammo_Decorator.h"
#include "AIController.h"
#include "Component/STU_Weapon_Component.h"
#include "STU_Utils.h"

USTU_Ammo_Decorator::USTU_Ammo_Decorator()
{
    NodeName = "Ammo need";
}

bool USTU_Ammo_Decorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp,
                                                     uint8*                  NodeMemory) const
{
    const AAIController* controller = OwnerComp.GetAIOwner();
    if (!controller)
        return false;

    const auto weapon_component =
        STU_Utils::get_STU_player_component<USTU_Weapon_Component>(controller->GetPawn());
    if (!weapon_component)
        return false;
    return weapon_component->need_ammo_to_reload(weapon_type);
}