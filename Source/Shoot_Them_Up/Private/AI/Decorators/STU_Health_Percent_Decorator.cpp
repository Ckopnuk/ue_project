// Shoot_Them_Up game from Ckopnuk

#include "AI/Decorators/STU_Health_Percent_Decorator.h"
#include "AIController.h"
#include "Component/STU_Helth_Component.h"
#include "STU_Utils.h"

USTU_Health_Percent_Decorator::USTU_Health_Percent_Decorator()
{
    NodeName = "Health Percent";
}

bool USTU_Health_Percent_Decorator::CalculateRawConditionValue(UBehaviorTreeComponent& owner_comp,
                                                               uint8* node_memory) const
{
    const AAIController* controller = owner_comp.GetAIOwner();
    if (!controller)
        return false;

    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(controller->GetPawn());

    if (!health_component || health_component->is_dead())
        return false;

    return health_component->get_helth_percent() <= health_percent_to_hill;
}