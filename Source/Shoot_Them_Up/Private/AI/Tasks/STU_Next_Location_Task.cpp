// Shoot_Them_Up game from Ckopnuk

#include "AI/Tasks/STU_Next_Location_Task.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"

USTU_Next_Location_Task::USTU_Next_Location_Task()
{
    NodeName = "Next Location";
}

EBTNodeResult::Type USTU_Next_Location_Task::ExecuteTask(UBehaviorTreeComponent& OwnerComp,
                                                         uint8*                  NodeMemory)
{
    const auto controller = OwnerComp.GetAIOwner();
    const auto blackboard = OwnerComp.GetBlackboardComponent();
    if (!controller || !blackboard)
        return EBTNodeResult::Failed;

    const auto pawn = controller->GetPawn();
    if (!pawn)
        return EBTNodeResult::Failed;

    const auto navigation_sistem = UNavigationSystemV1::GetCurrent(pawn);
    if (!navigation_sistem)
        return EBTNodeResult::Failed;

    FNavLocation nav_location;
    auto         location = pawn->GetActorLocation();
    if (!self_center)
    {
        auto center_actor =
            Cast<AActor>(blackboard->GetValueAsObject(center_actor_key.SelectedKeyName));
        if (!center_actor)
            return EBTNodeResult::Failed;
        location = center_actor->GetActorLocation();
    }

    const auto poin_found =
        navigation_sistem->GetRandomReachablePointInRadius(location, radius, nav_location);

    if (!poin_found)
        return EBTNodeResult::Failed;

    blackboard->SetValueAsVector(aim_location_key.SelectedKeyName, nav_location.Location);

    return EBTNodeResult::Succeeded;
}