// Shoot_Them_Up game from Ckopnuk

#include "AI/Service/STU_AI_Change_Weapon_Service.h"
#include "AIController.h"
#include "Component/STU_Weapon_Component.h"
#include "STU_Utils.h"

USTU_AI_Change_Weapon_Service::USTU_AI_Change_Weapon_Service()
{
    NodeName = "change_weapon";
}

void USTU_AI_Change_Weapon_Service::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory,
                                             float DeltaSeconds)
{
    const auto controller = OwnerComp.GetAIOwner();
    if (controller)
    {
        const auto weapon_componrnt =
            STU_Utils::get_STU_player_component<USTU_Weapon_Component>(controller->GetPawn());
        if (weapon_componrnt && probability > 0 && FMath::FRand() <= probability)
        {
            weapon_componrnt->next_weapon();
        }
    }
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}