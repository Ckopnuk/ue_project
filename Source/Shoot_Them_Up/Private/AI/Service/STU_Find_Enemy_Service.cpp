// Shoot_Them_Up game from Ckopnuk

#include "AI/Service/STU_Find_Enemy_Service.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Component/STU_AI_Perception_Component.h"
#include "STU_Utils.h"

USTU_Find_Enemy_Service::USTU_Find_Enemy_Service()
{
    NodeName = "find_enemy";
}

void USTU_Find_Enemy_Service::TickNode(UBehaviorTreeComponent& owner_comp, uint8* node_memory,
                                       float delta_seconds)
{
    const auto blackboard = owner_comp.GetBlackboardComponent();
    if (blackboard)
    {
        const auto controller = owner_comp.GetAIOwner();
        const auto perception_component =
            STU_Utils::get_STU_player_component<USTU_AI_Perception_Component>(controller);
        if (perception_component)
        {
            blackboard->SetValueAsObject(enemy_actor_key.SelectedKeyName,
                                         perception_component->get_closest_enemy());
        }
        Super::TickNode(owner_comp, node_memory, delta_seconds);
    }
}