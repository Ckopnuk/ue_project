// Shoot_Them_Up game from Ckopnuk

#include "AI/Service/STU_Fire_Service.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Component/STU_Weapon_Component.h"
#include "STU_Utils.h"

USTU_Fire_Service::USTU_Fire_Service()
{
    NodeName = "Fire";
}

void USTU_Fire_Service::TickNode(UBehaviorTreeComponent& owner_comp, uint8* node_memory,
                                 float delta_seconds)
{
    const auto controller = owner_comp.GetAIOwner();
    const auto blackboard = owner_comp.GetBlackboardComponent();

    const bool has_aim =
        blackboard && blackboard->GetValueAsObject(enemy_actor_key.SelectedKeyName);

    if (controller)
    {
        const auto weapon_component =
            STU_Utils::get_STU_player_component<USTU_Weapon_Component>(controller->GetPawn());
        if (weapon_component)
        {
            has_aim ? weapon_component->start_fire() : weapon_component->stop_fire();
        }
    }

    Super::TickNode(owner_comp, node_memory, delta_seconds);
}