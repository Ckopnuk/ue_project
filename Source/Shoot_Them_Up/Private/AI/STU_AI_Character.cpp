// Shoot_Them_Up game from Ckopnuk

#include "AI/STU_AI_Character.h"
#include "AI/STU_AI_Controller.h"
#include "BrainComponent.h"
#include "Component/STU_AI_Weapon_Component.h"
#include "Component/STU_Helth_Component.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UI/STU_Health_Bar_Widget.h"

ASTU_AI_Character::ASTU_AI_Character(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<USTU_AI_Weapon_Component>("weapon_component"))
{
    AutoPossessAI     = EAutoPossessAI::Disabled;
    AIControllerClass = ASTU_AI_Character::StaticClass();

    bUseControllerRotationYaw = false;
    if (GetCharacterMovement())
    {
        GetCharacterMovement()->RotationRate                  = FRotator(0.0f, 150.0f, 0.0f);
        GetCharacterMovement()->bUseControllerDesiredRotation = true;
    }

    health_widget_component = CreateDefaultSubobject<UWidgetComponent>("Health_Widget_Component");
    health_widget_component->SetupAttachment(GetRootComponent());
    health_widget_component->SetWidgetSpace(EWidgetSpace::Screen);
    health_widget_component->SetDrawAtDesiredSize(true);
}

void ASTU_AI_Character::BeginPlay()
{
    Super::BeginPlay();
    check(health_widget_component);
}

void ASTU_AI_Character::on_helth_change(float hp, float delta)
{
    Super::on_helth_change(hp, delta);

    const auto health_bar_widget =
        Cast<USTU_Health_Bar_Widget>(health_widget_component->GetUserWidgetObject());
    if (!health_bar_widget)
        return;

    health_bar_widget->set_health_percent(helth_component->get_helth_percent());
}

void ASTU_AI_Character::on_death()
{
    Super::on_death();

    const auto stu_controller = Cast<AAIController>(Controller);
    if (stu_controller && stu_controller->BrainComponent)
    {
        stu_controller->BrainComponent->Cleanup();
    }
}

void ASTU_AI_Character::update_health_widget_wisibility()
{
    if (!GetWorld() || !GetWorld()->GetFirstPlayerController() ||
        !GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator())
        return;

    const auto player_location =
        GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator ()->GetActorLocation();
    const auto distance = FVector::Distance(player_location, GetActorLocation());
    health_widget_component->SetVisibility(distance < health_visibility_distance, true);
}

void ASTU_AI_Character::Tick(float delta_time)
{
    Super::Tick(delta_time);
    update_health_widget_wisibility();
}
