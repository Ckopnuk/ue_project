// Shoot_Them_Up game from Ckopnuk

#include "AI/STU_AI_Controller.h"
#include "AI/STU_AI_Character.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Component/STU_AI_Perception_Component.h"
#include "Component/STU_Respawn_component.h"

ASTU_AI_Controller::ASTU_AI_Controller()
{
    stu_ai_perception_component =
        CreateDefaultSubobject<USTU_AI_Perception_Component>("STU_Perception_Component");

    respawn_component = CreateDefaultSubobject<USTU_Respawn_Component>("Respawn_Component");

    SetPerceptionComponent(*stu_ai_perception_component);
    bWantsPlayerState = true;
}

void ASTU_AI_Controller::OnPossess(APawn* in_pawn)
{
    Super::OnPossess(in_pawn);

    const auto stu_character = Cast<ASTU_AI_Character>(in_pawn);
    if (stu_character)
    {
        RunBehaviorTree(stu_character->behavior_tree_asset);
    }
}

void ASTU_AI_Controller::Tick(float delta_time)
{
    Super::Tick(delta_time);
    const auto aim_actor = get_focus_on_actor();
    SetFocus(aim_actor);
}

AActor* ASTU_AI_Controller::get_focus_on_actor() const
{
    if (!GetBlackboardComponent())
        return nullptr;
    return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(focus_on_key_name));
}