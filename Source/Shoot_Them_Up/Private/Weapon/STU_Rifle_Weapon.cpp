// Shoot_Them_Up game from Ckopnuk

#include "Weapon/STU_Rifle_Weapon.h"
#include "Components/AudioComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"
#include "Weapon/Components/STU_Weapon_VFX_Component.h"

// DEFINE_LOG_CATEGORY_STATIC(base_weapon_log, All, All)

ASTU_Rifle_Weapon::ASTU_Rifle_Weapon()
{
    weapon_fx_component = CreateDefaultSubobject<USTU_Weapon_VFX_Component>("weapon_fx_component");
}

void ASTU_Rifle_Weapon::BeginPlay()
{
    Super::BeginPlay();

    check(weapon_fx_component);
}

void ASTU_Rifle_Weapon::start_fire()
{
    init_fx();
    GetWorldTimerManager().SetTimer(shot_timer_handle, this, &ASTU_Rifle_Weapon::make_shot,
                                    time_between_shots, true);
    make_shot();
}

void ASTU_Rifle_Weapon::stop_fire()
{
    GetWorldTimerManager().ClearTimer(shot_timer_handle);
    set_fx_active(false);
}

void ASTU_Rifle_Weapon::zoom(bool enabled)
{
    const auto controller = Cast<APlayerController>(get_controller());
    if (!controller || !controller->PlayerCameraManager)
        return;

    if (enabled)
    {
        default_camera_fov = controller->PlayerCameraManager->GetFOVAngle();
    }

    controller->PlayerCameraManager->SetFOV(enabled ? fov_zoom_angle : default_camera_fov);
}

void ASTU_Rifle_Weapon::make_shot()
{
    if (!GetWorld())
    {
        // UE_LOG(base_weapon_log, Warning, TEXT("no world!!!"));
        return;
    }
    if (is_ammo_empty())
    {
        // UE_LOG(base_weapon_log, Warning, TEXT("no ammo!!!"));
        stop_fire();
        return;
    }

    FVector trace_start, trace_end;
    if (!get_trace_data(trace_start, trace_end))
    {
        stop_fire();
        return;
    }

    FHitResult hit_result;
    make_hit(hit_result, trace_start, trace_end);

    FVector trace_fx_end = trace_end;
    if (hit_result.bBlockingHit)
    {
        trace_fx_end = hit_result.ImpactPoint;
        make_dmg(hit_result);
        weapon_fx_component->play_impact_FX(hit_result);
    }

    spawn_trace_fx(get_muzzle_world_location(), trace_fx_end);
    decrease_ammo();
}

bool ASTU_Rifle_Weapon::get_trace_data(FVector& trace_start, FVector& trace_end) const
{
    FVector  view_location;
    FRotator view_rotation;
    if (!get_player_view_point(view_location, view_rotation))
        return false;

    trace_start         = view_location;
    const auto half_rad = FMath::DegreesToRadians(bullet_spread);

    const FVector trace_direction = FMath::VRandCone(view_rotation.Vector(), half_rad);
    trace_end                     = trace_start + trace_direction * trace_max_distance;

    return true;
}

void ASTU_Rifle_Weapon::make_dmg(const FHitResult& hit_result)
{
    AActor* enemy = hit_result.GetActor();
    if (!enemy)
        return;

    FPointDamageEvent point_damage_ivent;
    point_damage_ivent.HitInfo = hit_result;
    enemy->TakeDamage(damage_amount, point_damage_ivent, get_controller(), this);
}

void ASTU_Rifle_Weapon::init_fx()
{
    if (!muzzle_fx_component)
    {
        muzzle_fx_component = spawn_muzzle_fx();
    }

    fire_audio_component =
        UGameplayStatics::SpawnSoundAttached(fire_sound, weapon_mesh, muzzle_socket_name);
    set_fx_active(true);
}

void ASTU_Rifle_Weapon::set_fx_active(bool is_active)
{
    if (muzzle_fx_component)
    {
        muzzle_fx_component->SetPaused(!is_active);
        muzzle_fx_component->SetVisibility(is_active, true);
    }

    if (fire_audio_component)
    {
        is_active ? fire_audio_component->Play() : fire_audio_component->Stop();
    }
}

void ASTU_Rifle_Weapon::spawn_trace_fx(const FVector& trace_start, const FVector& trace_end)
{
    const auto trace_fx_component =
        UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), trace_fx, trace_start);
    if (trace_fx_component)
    {
        trace_fx_component->SetNiagaraVariableVec3(trace_target_name, trace_end);
    }
}

AController* ASTU_Rifle_Weapon::get_controller() const
{
    const auto pawn = Cast<APawn>(GetOwner());
    return pawn ? pawn->GetController() : nullptr;
}