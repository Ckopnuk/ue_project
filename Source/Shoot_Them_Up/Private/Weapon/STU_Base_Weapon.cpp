// Shoot_Them_Up game from Ckopnuk

#include "Weapon/STU_Base_Weapon.h"
#include "Components/AudioComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(base_weapon_log, All, All)

ASTU_Base_Weapon::ASTU_Base_Weapon()
{
    PrimaryActorTick.bCanEverTick = false;
    weapon_mesh                   = CreateDefaultSubobject<USkeletalMeshComponent>("weapon_mesh");
    SetRootComponent(weapon_mesh);
}

void ASTU_Base_Weapon::BeginPlay()
{

    Super::BeginPlay();

    check(weapon_mesh);
    checkf(default_ammo.bullets > 0, TEXT("Bullets count couldn't be less or equal zero!"));
    checkf(default_ammo.clips > 0, TEXT("Clips count couldn't be less or equal zero!"));

    current_ammo = default_ammo;
}

void ASTU_Base_Weapon::start_fire() {}

void ASTU_Base_Weapon::stop_fire() {}

void ASTU_Base_Weapon::make_shot() {}

bool ASTU_Base_Weapon::get_player_view_point(FVector& view_location, FRotator& view_rotation) const
{
    const auto stu_character = Cast<ACharacter>(GetOwner());
    if (!stu_character)
        return false;

    if (stu_character->IsPlayerControlled())
    {
        const auto controller = stu_character->GetController<APlayerController>();
        if (!controller)
        {
            UE_LOG(base_weapon_log, Warning, TEXT("get_player_view_point::no controller!!!"));
            return false;
        }

        controller->GetPlayerViewPoint(view_location, view_rotation);
    }
    else
    {
        view_location = get_muzzle_world_location();
        view_rotation = weapon_mesh->GetSocketRotation(muzzle_socket_name);
    }

    return true;
}

FVector ASTU_Base_Weapon::get_muzzle_world_location() const
{
    return weapon_mesh->GetSocketLocation(muzzle_socket_name);
}

bool ASTU_Base_Weapon::get_trace_data(FVector& trace_start, FVector& trace_end) const
{
    FVector  view_location;
    FRotator view_rotation;
    if (!get_player_view_point(view_location, view_rotation))
        return false;

    trace_start                   = view_location;
    const FVector trace_direction = view_rotation.Vector();
    trace_end                     = trace_start + trace_direction * trace_max_distance;

    return true;
}

void ASTU_Base_Weapon::make_hit(FHitResult& hit_result, const FVector& trace_start,
                                const FVector& trace_end)
{
    FCollisionQueryParams collision_params;
    collision_params.AddIgnoredActor(GetOwner());
    collision_params.bReturnPhysicalMaterial = true;
    GetWorld()->LineTraceSingleByChannel(hit_result, trace_start, trace_end,
                                         ECollisionChannel::ECC_Visibility, collision_params);
}

void ASTU_Base_Weapon::decrease_ammo()
{
    if (current_ammo.bullets == 0)
    {
        UE_LOG(base_weapon_log, Warning, TEXT("Clip is empty"));
        return;
    }

    current_ammo.bullets--;

    if (is_clip_empty() && !is_ammo_empty())
    {
        stop_fire();
        on_clip_empty.Broadcast(this);
    }
}

bool ASTU_Base_Weapon::is_ammo_empty() const
{
    return !current_ammo.infinite && current_ammo.clips == 0 && is_clip_empty();
}

bool ASTU_Base_Weapon::is_clip_empty() const
{
    return current_ammo.bullets == 0;
}

bool ASTU_Base_Weapon::is_ammo_full() const
{
    return current_ammo.clips == default_ammo.clips && current_ammo.bullets == default_ammo.bullets;
}

void ASTU_Base_Weapon::change_clip()
{
    if (!current_ammo.infinite)
    {
        if (current_ammo.clips == 0)
        {
            UE_LOG(base_weapon_log, Warning, TEXT("No more clips"));
            return;
        }
        current_ammo.clips--;
    }
    current_ammo.bullets = default_ammo.bullets;
    // UE_LOG(base_weapon_log, Display, TEXT("---------- CHANGE CLIP ----------"));
}

bool ASTU_Base_Weapon::can_reload() const
{
    return current_ammo.bullets < default_ammo.bullets && current_ammo.clips > 0;
}

bool ASTU_Base_Weapon::try_to_add_ammo(int32 clips_amount)
{
    if (current_ammo.infinite || is_ammo_full() || clips_amount <= 0)
        return false;

    if (is_ammo_empty())
    {
        current_ammo.clips = FMath::Clamp(clips_amount, 0, default_ammo.clips + 1);
        on_clip_empty.Broadcast(this);
    }
    else if (current_ammo.clips < default_ammo.clips)
    {
        const auto next_clips_amount = current_ammo.clips + clips_amount;
        if (default_ammo.clips - next_clips_amount >= 0)
        {
            current_ammo.clips = next_clips_amount;
        }
        else
        {
            current_ammo.clips   = default_ammo.clips;
            current_ammo.bullets = default_ammo.bullets;
        }
    }
    else
    {
        current_ammo.bullets = default_ammo.bullets;
        UE_LOG(base_weapon_log, Display, TEXT("Bullets is full now"));
    }
    return true;
}

void ASTU_Base_Weapon::log_ammo()
{
    FString ammo_info = "Ammo: " + FString::FromInt(current_ammo.bullets) + " / ";
    ammo_info += current_ammo.infinite ? "Infinite" : FString::FromInt(current_ammo.clips);
    UE_LOG(base_weapon_log, Display, TEXT("%s"), *ammo_info);
}

UNiagaraComponent* ASTU_Base_Weapon::spawn_muzzle_fx()
{
    return UNiagaraFunctionLibrary::SpawnSystemAttached(muzzle_fx,                     //
                                                        weapon_mesh,                   //
                                                        muzzle_socket_name,            //
                                                        FVector::ZeroVector,           //
                                                        FRotator::ZeroRotator,         //
                                                        EAttachLocation::SnapToTarget, //
                                                        true);
}
