// Shoot_Them_Up game from Ckopnuk

#include "Weapon/STU_Lauuncher_Weapon.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Weapon/STU_Projectile.h"

void ASTU_Lauuncher_Weapon::start_fire()
{
    make_shot();
}

void ASTU_Lauuncher_Weapon::make_shot()
{
    if (!GetWorld())
    {

        return;
    }

    if (is_ammo_empty())
    {
        stop_fire();
        UGameplayStatics::SpawnSoundAtLocation(GetWorld(), no_ammo_sound, GetActorLocation());
        return;
    }

    FVector trace_start, trace_end;
    if (!get_trace_data(trace_start, trace_end))
    {
        stop_fire();
        return;
    }

    FHitResult hit_result;
    make_hit(hit_result, trace_start, trace_end);

    const FVector end_point = hit_result.bBlockingHit ? hit_result.ImpactPoint : trace_end;
    const FVector direction = (end_point - get_muzzle_world_location()).GetSafeNormal();

    const FTransform spawn_transform(FRotator::ZeroRotator, get_muzzle_world_location());

    ASTU_Projectile* projectile =
        GetWorld()->SpawnActorDeferred<ASTU_Projectile>(projectile_class, spawn_transform);

    if (projectile)
    {
        projectile->set_shot_direction(direction);
        projectile->SetOwner(GetOwner());
        projectile->FinishSpawning(spawn_transform);
    }

    decrease_ammo();
    spawn_muzzle_fx();
    UGameplayStatics::SpawnSoundAttached(fire_sound, weapon_mesh, muzzle_socket_name);
}
