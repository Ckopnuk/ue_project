// Shoot_Them_Up game from Ckopnuk

#include "Weapon/Components/STU_Weapon_VFX_Component.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"

// Sets default values for this component's properties
USTU_Weapon_VFX_Component::USTU_Weapon_VFX_Component()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void USTU_Weapon_VFX_Component::play_impact_FX(const FHitResult& hit)
{
    auto impact_data = default_impact_data;

    if (hit.PhysMaterial.IsValid())
    {
        const auto phys_mat = hit.PhysMaterial.Get();
        if (impact_data_map.Contains(phys_mat))
        {
            impact_data = impact_data_map[phys_mat];
        }
    }

    // niagera spawn
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), impact_data.niagara_effect,
                                                   hit.ImpactPoint, hit.ImpactNormal.Rotation());

    // decal spawn
    auto decal_component = UGameplayStatics::SpawnDecalAtLocation(
        GetWorld(), impact_data.decal_data.material, impact_data.decal_data.size, hit.ImpactPoint,
        hit.ImpactNormal.Rotation());
    if (decal_component)
    {
        decal_component->SetFadeOut(impact_data.decal_data.life_time,
                                    impact_data.decal_data.fade_out_time);
    }

    // sound
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), impact_data.sound, hit.ImpactPoint);
}

// Called when the game starts
void USTU_Weapon_VFX_Component::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void USTU_Weapon_VFX_Component::TickComponent(float DeltaTime, ELevelTick TickType,
                                              FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
