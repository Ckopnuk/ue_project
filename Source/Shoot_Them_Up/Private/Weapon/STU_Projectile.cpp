// Shoot_Them_Up game from Ckopnuk

#include "Weapon/STU_Projectile.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Weapon/Components/STU_Weapon_VFX_Component.h"

ASTU_Projectile::ASTU_Projectile()
{
    PrimaryActorTick.bCanEverTick = false;

    collision_component = CreateDefaultSubobject<USphereComponent>("sphere_component");
    collision_component->InitSphereRadius(5.0);
    collision_component->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    collision_component->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    collision_component->bReturnMaterialOnMove = true;

    SetRootComponent(collision_component);

    movement_component =
        CreateDefaultSubobject<UProjectileMovementComponent>("projectile_movement_component");
    movement_component->InitialSpeed           = 2000;
    movement_component->ProjectileGravityScale = 0.5;

    weapon_fx_component = CreateDefaultSubobject<USTU_Weapon_VFX_Component>("weapon_fx_component");
}

void ASTU_Projectile::BeginPlay()
{
    Super::BeginPlay();

    check(movement_component);
    check(collision_component);
    check(weapon_fx_component);

    movement_component->Velocity = shot_direction * movement_component->InitialSpeed;
    collision_component->IgnoreActorWhenMoving(GetOwner(), true);
    collision_component->OnComponentHit.AddDynamic(this, &ASTU_Projectile::on_projectile_hit);

    SetLifeSpan(life_seconds);
}

void ASTU_Projectile::on_projectile_hit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                        const FHitResult& Hit)
{
    if (!GetWorld())
        return;

    movement_component->StopMovementImmediately();

    // make damage
    UGameplayStatics::ApplyRadialDamage(GetWorld(), damage_amount, GetActorLocation(),
                                        damage_radius, UDamageType::StaticClass(), { GetOwner() },
                                        this, get_controller(), du_full_damage);

    //  DrawDebugSphere(GetWorld(), GetActorLocation(), damage_radius, 32, FColor::Red,
    //  false, 5.0f);
    weapon_fx_component->play_impact_FX(Hit);
    Destroy();
}

AController* ASTU_Projectile::get_controller() const
{
    const auto pawn = Cast<APawn>(GetOwner());
    return pawn ? pawn->GetController() : nullptr;
}
