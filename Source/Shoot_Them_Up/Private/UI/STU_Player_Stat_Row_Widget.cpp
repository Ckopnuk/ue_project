// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Player_Stat_Row_Widget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

void USTU_Player_Stat_Row_Widget::set_player_name(const FText& text)
{
    if (!player_name_text_block)
        return;
    player_name_text_block->SetText(text);
}

void USTU_Player_Stat_Row_Widget::set_kills(const FText& text)
{
    if (!kills_text_block)
        return;
    kills_text_block->SetText(text);
}

void USTU_Player_Stat_Row_Widget::set_deaths(const FText& text)
{
    if (!deaths_text_block)
        return;
    deaths_text_block->SetText(text);
}

void USTU_Player_Stat_Row_Widget::set_team(const FText& text)
{
    if (!team_text_block)
        return;
    team_text_block->SetText(text);
}

void USTU_Player_Stat_Row_Widget::set_player_indicator_visibility(bool visible)
{
    if (!player_indicator_image)
        return;
    player_indicator_image->SetVisibility(visible ? ESlateVisibility::Visible
                                                  : ESlateVisibility::Hidden);
}

void USTU_Player_Stat_Row_Widget::set_team_color(const FLinearColor& color)
{
    if (!team_image)
        return;
    team_image->SetColorAndOpacity(color);
}
