// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Health_Bar_Widget.h"
#include "Components/ProgressBar.h"

void USTU_Health_Bar_Widget::set_health_percent(float percent)
{
    if (!health_progress_bar)
        return;

    const auto health_bar_visibility =
        (percent > percent_visibility_threshold || FMath::IsNearlyZero(percent))
            ? ESlateVisibility::Hidden
            : ESlateVisibility::Visible;
    health_progress_bar->SetVisibility(health_bar_visibility);

    const auto health_bar_color = percent > percent_color_threshold ? good_color : bad_color;
    health_progress_bar->SetFillColorAndOpacity(health_bar_color);

    health_progress_bar->SetPercent(percent);
}