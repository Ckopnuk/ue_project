// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Go_To_Menu_Widget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "STU_Game_Instance.h"

//DEFINE_LOG_CATEGORY_STATIC(log_STU_widget, All, All);

void USTU_Go_To_Menu_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (go_menu_button)
    {
        go_menu_button->OnClicked.AddDynamic(this, &USTU_Go_To_Menu_Widget::on_go_to_menu);
    }
}

void USTU_Go_To_Menu_Widget::on_go_to_menu()
{
    if (!GetWorld())
        return;

    const auto stu_game_instance = GetWorld()->GetGameInstance<USTU_Game_Instance>();
    if (!stu_game_instance)
        return;

    if (stu_game_instance->get_menu_level_name().IsNone())
    {
//        UE_LOG(log_STU_widget, Error, TEXT("Level name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, stu_game_instance->get_menu_level_name());
}
