// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Game_Data_Widget.h"
#include "Player/STU_Player_State.h"
#include "STU_GameModeBase.h"


int32 USTU_Game_Data_Widget::get_current_round() const
{
    const auto game_mode = get_stu_game_mode();
    return game_mode ? game_mode->get_current_round() : 0;
}

int32 USTU_Game_Data_Widget::get_total_rounds_num() const
{
    const auto game_mode = get_stu_game_mode();
    return game_mode ? game_mode->get_game_data().rounds_num : 0;
}

int32 USTU_Game_Data_Widget::get_round_seconds_remaining() const
{
    const auto game_mode = get_stu_game_mode();
    return game_mode ? game_mode->get_round_seconds_remaining() : 0;
}

ASTU_Player_State* USTU_Game_Data_Widget::get_stu_player_state() const
{
    return GetOwningPlayer() ? Cast<ASTU_Player_State>(GetOwningPlayer()->PlayerState) : nullptr;
}

ASTU_GameModeBase* USTU_Game_Data_Widget::get_stu_game_mode() const
{
    return GetWorld() ? Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}
