// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Game_Over_Widget.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"
#include "Kismet/GameplayStatics.h"
#include "Player/STU_Player_State.h"
#include "STU_GameModeBase.h"
#include "STU_Utils.h"
#include "UI/STU_Player_Stat_Row_Widget.h"

void USTU_Game_Over_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GetWorld())
    {
        const auto game_mode = Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode());
        if (game_mode)
        {
            game_mode->on_match_changed.AddUObject(this,
                                                   &USTU_Game_Over_Widget::on_match_state_changed);
        }
    }

    if (reset_level_button)
    {
        reset_level_button->OnClicked.AddDynamic(this, &USTU_Game_Over_Widget::on_rest_level);
    }
}

void USTU_Game_Over_Widget::on_match_state_changed(ESTU_Match_State state)
{
    if (state == ESTU_Match_State::game_over)
    {
        update_player_state();
    }
}

void USTU_Game_Over_Widget::update_player_state()
{
    if (!GetWorld() || !player_stat_box)
        return;

    player_stat_box->ClearChildren();

    for (auto it = GetWorld()->GetControllerIterator(); it; ++it)
    {
        const auto controller = it->Get();
        if (!controller)
            continue;

        const auto player_state = Cast<ASTU_Player_State>(controller->PlayerState);
        if (!player_state)
            continue;

        const auto player_stat_row_widget =
            CreateWidget<USTU_Player_Stat_Row_Widget>(GetWorld(), player_stat_row_widget_class);
        if (!player_stat_row_widget)
            continue;

        player_stat_row_widget->set_player_name(FText::FromString(player_state->GetPlayerName()));
        player_stat_row_widget->set_kills(STU_Utils::text_from_int(player_state->get_kills_num()));
        player_stat_row_widget->set_deaths(
            STU_Utils::text_from_int(player_state->get_deaths_nums()));
        player_stat_row_widget->set_team(STU_Utils::text_from_int(player_state->get_team_id()));
        player_stat_row_widget->set_player_indicator_visibility(controller->IsPlayerController());
        player_stat_row_widget->set_team_color(player_state->get_team_color());

        player_stat_box->AddChild(player_stat_row_widget);
    }
}

void USTU_Game_Over_Widget::on_rest_level()
{
    const FString current_level_name = UGameplayStatics::GetCurrentLevelName(this);

    UGameplayStatics::OpenLevel(this, FName(current_level_name));
}
