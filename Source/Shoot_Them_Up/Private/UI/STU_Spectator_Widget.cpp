// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Spectator_Widget.h"
#include "Component/STU_Respawn_Component.h"
#include "STU_Utils.h"

bool USTU_Spectator_Widget::get_respawn_time(int32& count_down_time) const
{
    const auto respawn_componennt =
        STU_Utils::get_STU_player_component<USTU_Respawn_Component>(GetOwningPlayer());
    if (!respawn_componennt || !respawn_componennt->is_respawn_in_progress())
        return false;

    count_down_time = respawn_componennt->get_respawn_count_doun();
    return true;
}