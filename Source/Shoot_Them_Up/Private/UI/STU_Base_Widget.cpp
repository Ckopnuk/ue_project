// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Base_Widget.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

void USTU_Base_Widget::show()
{
    PlayAnimation(show_animation);
    UGameplayStatics::PlaySound2D(GetWorld(), open_sound);
}