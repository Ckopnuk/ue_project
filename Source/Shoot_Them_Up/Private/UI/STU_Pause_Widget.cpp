// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Pause_Widget.h"
#include "Components/Button.h"
#include "Gameframework/GameModeBase.h"

void USTU_Pause_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (clear_pause_button)
    {
        clear_pause_button->OnClicked.AddDynamic(this, &USTU_Pause_Widget::on_clear_pause);
    }

}

void USTU_Pause_Widget::on_clear_pause()
{
    if (!GetWorld() || !GetWorld()->GetAuthGameMode())
        return;

    GetWorld()->GetAuthGameMode()->ClearPause();
}
