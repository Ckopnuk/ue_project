// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Game_HUD.h"
#include "Engine/Canvas.h"
#include "STU_GameModeBase.h"
#include "UI/STU_Base_Widget.h"

DEFINE_LOG_CATEGORY_STATIC(Log_STU_Game_HUD, All, All);

void ASTU_Game_HUD::DrawHUD()
{
    Super::DrawHUD();

    // draw_cross_hair();
}

void ASTU_Game_HUD::BeginPlay()
{
    Super::BeginPlay();

    game_widgets.Add(ESTU_Match_State::im_progress,
                     CreateWidget<USTU_Base_Widget>(GetWorld(), player_HUD_widget_class));
    game_widgets.Add(ESTU_Match_State::pause,
                     CreateWidget<USTU_Base_Widget>(GetWorld(), pause_widget_class));
    game_widgets.Add(ESTU_Match_State::game_over,
                     CreateWidget<USTU_Base_Widget>(GetWorld(), game_over_widget_class));

    for (auto game_widget_pair : game_widgets)
    {
        const auto game_widget = game_widget_pair.Value;
        if (!game_widget)
            continue;

        game_widget->AddToViewport();
        game_widget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GetWorld())
    {
        const auto game_mode = Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode());
        if (game_mode)
        {
            game_mode->on_match_changed.AddUObject(this, &ASTU_Game_HUD::on_match_state_changed);
        }
    }
}

void ASTU_Game_HUD::draw_cross_hair()
{
    const float half_line_size = 10.0f;
    const float line_thicknes  = 2.0f;

    const TInterval<float> center(Canvas->SizeX * 0.5, Canvas->SizeY * 0.5);
    const FLinearColor     liner_color = FLinearColor::Green;

    DrawLine(center.Min - half_line_size, center.Max, center.Min + half_line_size, center.Max,
             liner_color, line_thicknes);
    DrawLine(center.Min, center.Max - half_line_size, center.Min, center.Max + half_line_size,
             liner_color, line_thicknes);
}

void ASTU_Game_HUD::on_match_state_changed(ESTU_Match_State state)
{
    if (current_widget)
    {
        current_widget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (game_widgets.Contains(state))
    {
        current_widget = game_widgets[state];
    }

    if (current_widget)
    {
        current_widget->SetVisibility(ESlateVisibility::Visible);
        current_widget->show();
    }

    UE_LOG(Log_STU_Game_HUD, Display, TEXT("Match state changed: %s"),
           *UEnum::GetValueAsString(state));
}
