// Shoot_Them_Up game from Ckopnuk

#include "UI/STU_Player_Widget.h"
#include "Component/STU_Helth_Component.h"
#include "Component/STU_Weapon_Component.h"
#include "Components/ProgressBar.h"
#include "Player/STU_Player_State.h"
#include "STU_Utils.h"

float USTU_Player_Widget::get_helth_percent() const
{
    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(GetOwningPlayerPawn());
    if (!health_component)
        return 0.0f;

    return health_component->get_helth_percent();
}

bool USTU_Player_Widget::get_current_weapon_UI_data(FWeapon_UI_Data& ui_data) const
{
    const auto weapon_component =
        STU_Utils::get_STU_player_component<USTU_Weapon_Component>(GetOwningPlayerPawn());
    if (!weapon_component)
        return false;

    return weapon_component->get_current_weapon_UI_data(ui_data);
}

bool USTU_Player_Widget::geet_current_weapon_ammo_data(FAmmo_Data& ammo_data) const
{

    const auto weapon_component =
        STU_Utils::get_STU_player_component<USTU_Weapon_Component>(GetOwningPlayerPawn());
    if (!weapon_component)
        return false;

    return weapon_component->get_current_weapon_ammo_data(ammo_data);
}

bool USTU_Player_Widget::is_player_alive() const
{
    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(GetOwningPlayerPawn());
    return health_component && !health_component->is_dead();
}

bool USTU_Player_Widget::is_player_spectcting() const
{
    const auto controller = GetOwningPlayer();
    return controller && controller->GetStateName() == NAME_Spectating;
}

void USTU_Player_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();
    if (GetOwningPlayer())
    {
        GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this,
                                                             &USTU_Player_Widget::on_new_pawn);
        on_new_pawn(GetOwningPlayerPawn());
    }
}

void USTU_Player_Widget::on_health_changed(float hp, float delta)
{
    if (delta < 0.0f)
    {
        on_take_damage();

        if (!IsAnimationPlaying(damage_animation))
        {
        
        PlayAnimation(damage_animation);
        }
    }

    update_health_bar();
}

void USTU_Player_Widget::on_new_pawn(APawn* new_pawn)
{
    const auto health_component =
        STU_Utils::get_STU_player_component<USTU_Helth_Component>(new_pawn);
    if (health_component && !health_component->on_helth_change.IsBoundToObject(this))
    {
        health_component->on_helth_change.AddUObject(this, &USTU_Player_Widget::on_health_changed);
    }

    update_health_bar();
}

void USTU_Player_Widget::update_health_bar()
{
    if (health_progress_bar)
    {
        health_progress_bar->SetFillColorAndOpacity(
            get_helth_percent() > percent_color_threshold ? good_color : bad_color);
    }
}

int32 USTU_Player_Widget::get_kills_num() const
{
    const auto controller = GetOwningPlayer();
    if (!controller)
        return 0;

    const auto player_state = Cast<ASTU_Player_State>(controller->PlayerState);
    return player_state ? player_state->get_kills_num() : 0;
}
