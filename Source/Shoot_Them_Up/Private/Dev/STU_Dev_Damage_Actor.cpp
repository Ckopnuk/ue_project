// Shoot_Them_Up game from Ckopnuk

#include "Dev/STU_Dev_Damage_Actor.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASTU_Dev_Damage_Actor::ASTU_Dev_Damage_Actor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to
    // improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    scene_component =
        CreateDefaultSubobject<USceneComponent>("scene_component");
    SetRootComponent(scene_component);
}

// Called when the game starts or when spawned
void ASTU_Dev_Damage_Actor::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void ASTU_Dev_Damage_Actor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    DrawDebugSphere(GetWorld(), GetActorLocation(), radius, 24, sphere_color);
    UGameplayStatics::ApplyRadialDamage(GetWorld(), damage, GetActorLocation(),
                                        radius, damage_type, {}, this, nullptr,
                                        du_full_damage);
}
