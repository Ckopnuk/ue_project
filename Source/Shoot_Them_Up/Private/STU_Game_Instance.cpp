// Shoot_Them_Up game from Ckopnuk

#include "STU_Game_Instance.h"
#include "Sound/STU_Sound_Func_Lib.h"

void USTU_Game_Instance::toggle_volume()
{
    USTU_Sound_Func_Lib::toggle_sound_class_volume(master_sound_class);
}
