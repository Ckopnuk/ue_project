// Shoot_Them_Up game from Ckopnuk

#include "Sound/STU_Sound_Func_Lib.h"
#include "Sound/SoundClass.h"

DEFINE_LOG_CATEGORY_STATIC(log_stu_sound, All, All)

void USTU_Sound_Func_Lib::set_sound_class_volume(USoundClass* sound_class, float volume)
{
    if (!sound_class)
        return;

    sound_class->Properties.Volume = FMath::Clamp(volume, 0.0f, 1.0f);
    UE_LOG(log_stu_sound, Display, TEXT("Sound class volume was changed: %s = %f"),
           *sound_class->GetName(), sound_class->Properties.Volume);
}

void USTU_Sound_Func_Lib::toggle_sound_class_volume(USoundClass* sound_class)
{
    if (!sound_class)
        return;

    const float next_volume = sound_class->Properties.Volume > 0.0f ? 0.0f : 1.0f;
    set_sound_class_volume(sound_class, next_volume);
}
