// Shoot_Them_Up game from Ckopnuk

#include "Menu/STU_Menu_Game_Mode_Base.h"
#include "Menu/STU_Menu_HUD.h"
#include "Menu/STU_Menu_Player_Controller.h"

ASTU_Menu_Game_Mode_Base::ASTU_Menu_Game_Mode_Base()
{
    PlayerControllerClass = ASTU_Menu_Player_Controller::StaticClass();
    HUDClass              = ASTU_Menu_HUD::StaticClass();
}