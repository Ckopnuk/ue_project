// Shoot_Them_Up game from Ckopnuk

#include "Menu/UI/STU_Menu_Widget.h"
#include "Components/Button.h"
#include "Components/HorizontalBox.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h "
#include "Menu/UI/STU_Level_Item_Widget.h"
#include "STU_Game_Instance.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(log_STU_widget, All, All);

void USTU_Menu_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (start_game_button)
    {
        start_game_button->OnClicked.AddDynamic(this, &USTU_Menu_Widget::on_start_game);
    }

    if (quit_game_button)
    {
        quit_game_button->OnClicked.AddDynamic(this, &USTU_Menu_Widget::on_quit_game);
    }

    init_level_items();
}

void USTU_Menu_Widget::OnAnimationFinished_Implementation(const UWidgetAnimation* animation)
{
    if (animation != hide_animation)
        return;

    const auto stu_game_instance = get_stu_game_instance();
    if (!stu_game_instance)
        return;

    UGameplayStatics::OpenLevel(this, stu_game_instance->get_startup_level().level_name);
}

void USTU_Menu_Widget::on_quit_game()
{
    UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void USTU_Menu_Widget::on_start_game()
{
    PlayAnimation(hide_animation);
    UGameplayStatics::PlaySound2D(GetWorld(), start_game_sound);
}

void USTU_Menu_Widget::init_level_items()
{
    const auto stu_game_instance = get_stu_game_instance();
    if (!stu_game_instance)
        return;

    checkf(stu_game_instance->get_levels_data().Num() != 0, TEXT("levels data must not be empty!"));

    if (!level_items_box)
        return;

    level_items_box->ClearChildren();

    for (auto level_data : stu_game_instance->get_levels_data())
    {
        const auto level_item_widget =
            CreateWidget<USTU_Level_Item_Widget>(GetWorld(), level_item_widget_class);
        if (!level_item_widget)
            continue;

        level_item_widget->set_level_data(level_data);
        level_item_widget->on_level_selected.AddUObject(this, &USTU_Menu_Widget::on_level_selected);

        level_items_box->AddChild(level_item_widget);
        level_item_widgets.Add(level_item_widget);
    }

    if (stu_game_instance->get_startup_level().level_name.IsNone())
    {
        on_level_selected(stu_game_instance->get_levels_data()[0]);
    }
    else
    {
        on_level_selected(stu_game_instance->get_startup_level());
    }
}

void USTU_Menu_Widget::on_level_selected(const FLevel_Data& data)
{
    const auto stu_game_instance = get_stu_game_instance();
    if (!stu_game_instance)
        return;

    stu_game_instance->set_startup_level(data);

    for (auto level_item_widget : level_item_widgets)
    {
        if (level_item_widget)
        {
            const auto is_selected =
                data.level_name == level_item_widget->get_level_data().level_name;
            level_item_widget->set_selected(is_selected);
        }
    }
}

USTU_Game_Instance* USTU_Menu_Widget::get_stu_game_instance() const
{
    if (!GetWorld())
        return nullptr;

    return GetWorld()->GetGameInstance<USTU_Game_Instance>();
}
