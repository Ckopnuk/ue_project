// Shoot_Them_Up game from Ckopnuk

#include "Menu/UI/STU_Level_Item_Widget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

void USTU_Level_Item_Widget::on_level_item_clicked()
{
    on_level_selected.Broadcast(level_data);
}

void USTU_Level_Item_Widget::set_level_data(const FLevel_Data& data)
{
    level_data = data;
    if (level_name_text_block)
    {
        level_name_text_block->SetText(FText::FromName(data.level_display_name));
    }

    if (level_image)
    {
        level_image->SetBrushFromTexture(data.level_thumb);
    }
}

void USTU_Level_Item_Widget::set_selected(bool is_selected)
{
    if (level_image)
    {
        level_image->SetColorAndOpacity(is_selected ? FLinearColor::Red : FLinearColor::White);
    }
}

void USTU_Level_Item_Widget::on_level_item_hovered()
{
    if (frame_image)
    {
        frame_image->SetVisibility(ESlateVisibility::Visible);
    }
}

void USTU_Level_Item_Widget::on_level_item_unhovered()
{
    if (frame_image)
    {
        frame_image->SetVisibility(ESlateVisibility::Hidden);
    }
}
void USTU_Level_Item_Widget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (level_select_button)
    {
        level_select_button->OnClicked.AddDynamic(this,
                                                  &USTU_Level_Item_Widget::on_level_item_clicked);
        level_select_button->OnHovered.AddDynamic(this,
                                                  &USTU_Level_Item_Widget::on_level_item_hovered);
        level_select_button->OnUnhovered.AddDynamic(
            this, &USTU_Level_Item_Widget::on_level_item_unhovered);
    }
}