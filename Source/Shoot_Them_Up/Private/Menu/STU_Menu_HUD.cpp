// Shoot_Them_Up game from Ckopnuk

#include "Menu/STU_Menu_HUD.h"
#include "UI/STU_Base_Widget.h"

void ASTU_Menu_HUD::BeginPlay()
{
    Super::BeginPlay();

    const auto menu_widget = CreateWidget<USTU_Base_Widget>(GetWorld(), menu_widget_class);
    if (menu_widget)
    {
        menu_widget->AddToViewport();
        menu_widget->show();
    }
}
