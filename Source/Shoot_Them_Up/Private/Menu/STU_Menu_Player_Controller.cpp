// Shoot_Them_Up game from Ckopnuk

#include "Menu/STU_Menu_Player_Controller.h"
#include "STU_Game_Instance.h"

void ASTU_Menu_Player_Controller::BeginPlay()
{
    Super::BeginPlay();

    SetInputMode(FInputModeUIOnly());
    bShowMouseCursor = true;
}