// Shoot_Them_Up game from Ckopnuk

#include "Player/STU_Player_Character.h"
#include "Camera/CameraComponent.h"
#include "Component/STU_Weapon_Component.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/SpringArmComponent.h"

ASTU_Player_Character::ASTU_Player_Character(const FObjectInitializer& ObInit)
    : Super(ObInit)
{
    PrimaryActorTick.bCanEverTick = true;

    spring_arm_component = CreateDefaultSubobject<USpringArmComponent>("spring_arm_component");
    spring_arm_component->SetupAttachment(GetRootComponent());
    spring_arm_component->bUsePawnControlRotation = true;
    spring_arm_component->SocketOffset            = FVector(0.0f, 65.0f, 90.0f);

    camera_component = CreateDefaultSubobject<UCameraComponent>("camera_component");
    camera_component->SetupAttachment(spring_arm_component);

    camera_collision_component = CreateDefaultSubobject<USphereComponent>("USphereComponent");
    camera_collision_component->SetupAttachment(camera_component);
    camera_collision_component->SetSphereRadius(10.0f);
    camera_collision_component->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void ASTU_Player_Character::BeginPlay()
{
    Super::BeginPlay();
    check(camera_collision_component);

    camera_collision_component->OnComponentBeginOverlap.AddDynamic(
        this, &ASTU_Player_Character::on_camera_collision_begin_overlap);
    camera_collision_component->OnComponentEndOverlap.AddDynamic(
        this, &ASTU_Player_Character::on_camera_collision_end_overlap);
}

void ASTU_Player_Character::on_death()
{
    Super::on_death();
    if (Controller)
    {
        Controller->ChangeState(NAME_Spectating);
    }
}

void ASTU_Player_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    check(PlayerInputComponent);
    check(weapon_component);

    PlayerInputComponent->BindAxis("move_forward", this, &ASTU_Player_Character::move_forward);
    PlayerInputComponent->BindAxis("move_right", this, &ASTU_Player_Character::move_right);
    PlayerInputComponent->BindAxis("look_up", this,
                                   &ASTU_Player_Character::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("turn_around", this,
                                   &ASTU_Player_Character::AddControllerYawInput);
    PlayerInputComponent->BindAction("jump", IE_Pressed, this, &ASTU_Player_Character::Jump);
    PlayerInputComponent->BindAction("run", IE_Pressed, this, &ASTU_Player_Character::on_start_run);
    PlayerInputComponent->BindAction("run", IE_Released, this,
                                     &ASTU_Player_Character::on_finish_run);
    PlayerInputComponent->BindAction("fire", IE_Pressed, weapon_component,
                                     &USTU_Weapon_Component::start_fire);
    PlayerInputComponent->BindAction("fire", IE_Released, weapon_component,
                                     &USTU_Weapon_Component::stop_fire);
    PlayerInputComponent->BindAction("next_weapon", IE_Released, weapon_component,
                                     &USTU_Weapon_Component::next_weapon);
    PlayerInputComponent->BindAction("reload", IE_Pressed, weapon_component,
                                     &USTU_Weapon_Component::reload);
    DECLARE_DELEGATE_OneParam(FZoom_input_sigature, bool);
    PlayerInputComponent->BindAction<FZoom_input_sigature>("zoom", IE_Pressed, weapon_component,
                                                           &USTU_Weapon_Component::zoom, true);
    PlayerInputComponent->BindAction<FZoom_input_sigature>("zoom", IE_Released, weapon_component,
                                                           &USTU_Weapon_Component::zoom, false);
}

bool ASTU_Player_Character::is_running() const
{
    return wants_to_run && is_move_forward && !GetVelocity().IsZero();
}

void ASTU_Player_Character::on_camera_collision_begin_overlap(
    UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    check_camera_overlap();
}

void ASTU_Player_Character::on_camera_collision_end_overlap(
    UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex)
{
    check_camera_overlap();
}

void ASTU_Player_Character::check_camera_overlap()
{
    const bool hide_mesh =
        camera_collision_component->IsOverlappingComponent(GetCapsuleComponent());
    GetMesh()->SetOwnerNoSee(hide_mesh);

    TArray<USceneComponent*> mesh_children;

    GetMesh()->GetChildrenComponents(true, mesh_children);

    for (auto mesh : mesh_children)
    {
        const auto mesh_geometry = Cast<UPrimitiveComponent>(mesh);
        if (mesh_geometry)
        {
            mesh_geometry->SetOwnerNoSee(hide_mesh);
        }
    }
}

void ASTU_Player_Character::move_forward(float amount)
{
    if (range >= 0)
    {
        is_move_forward = amount > 0.0f;
        if (amount == 0.0f)
            return;
        AddMovementInput(GetActorForwardVector(), amount);
        // range -= amount;
    }
}

void ASTU_Player_Character::move_right(float amount)
{
    if (range >= 0)
    {
        if (amount == 0.0f)
            return;
        AddMovementInput(GetActorRightVector(), amount);
        // range -= amount;
    }
}

void ASTU_Player_Character::on_start_run()
{
    wants_to_run = true;
}

void ASTU_Player_Character::on_finish_run()
{
    wants_to_run = false;
}