// Shoot_Them_Up game from Ckopnuk

#include "Player/STU_PlayerController.h"
#include "Component/STU_Respawn_component.h"
#include "STU_Game_Instance.h"

#include "STU_GameModeBase.h"

ASTU_PlayerController::ASTU_PlayerController()
{
    respawn_component = CreateDefaultSubobject<USTU_Respawn_Component>("Respawn_Component");
}

void ASTU_PlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld())
    {
        const auto game_mode = Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode());
        if (game_mode)
        {
            game_mode->on_match_changed.AddUObject(this,
                                                   &ASTU_PlayerController::on_match_state_changed);
        }
    }
}

void ASTU_PlayerController::OnPossess(APawn* in_pawn)
{
    Super::OnPossess(in_pawn);
}

void ASTU_PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    if (!InputComponent)
        return;

    InputComponent->BindAction("pause_game", IE_Pressed, this,
                               &ASTU_PlayerController::on_pause_game);
    InputComponent->BindAction("mute", IE_Pressed, this, &ASTU_PlayerController::on_mute_sound);
}

void ASTU_PlayerController::on_pause_game()
{
    if (!GetWorld() || !GetWorld()->GetAuthGameMode())
        return;

    GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ASTU_PlayerController::on_match_state_changed(ESTU_Match_State state)
{
    if (state == ESTU_Match_State::im_progress)
    {
        SetInputMode(FInputModeGameOnly());
        bShowMouseCursor = false;
    }
    else
    {
        SetInputMode(FInputModeUIOnly());
        bShowMouseCursor = true;
    }
}

void ASTU_PlayerController::on_mute_sound()
{
    if (!GetWorld())
        return;

    const auto stu_game_instance = GetWorld()->GetGameInstance<USTU_Game_Instance>();
    if (!stu_game_instance)
        return;

    stu_game_instance->toggle_volume();
}
