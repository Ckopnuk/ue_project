// Shoot_Them_Up game from Ckopnuk

#include "Player/STU_Player_State.h"

DEFINE_LOG_CATEGORY_STATIC(log_stu_player_state, All, All)

void ASTU_Player_State::log_info()
{
    UE_LOG(log_stu_player_state, Display, TEXT("Team ID: %i, Kills: %i, Death: %i"), team_id,
           kills_num, deaths_num);
}
