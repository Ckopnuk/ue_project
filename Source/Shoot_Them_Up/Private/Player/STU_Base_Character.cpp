// Shoot_Them_Up game from Ckopnuk

#include "Player/STU_Base_Character.h"
#include "Component/STU_CharacterMovementComponent.h"
#include "Component/STU_Helth_Component.h"
#include "Component/STU_Weapon_Component.h"
#include "Components/CapsuleComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(base_character_log, All, All)

// Sets default values
ASTU_Base_Character::ASTU_Base_Character(const FObjectInitializer& ObInit)
    : Super(ObInit.SetDefaultSubobjectClass<USTU_CharacterMovementComponent>(
          ACharacter::CharacterMovementComponentName))
{
    PrimaryActorTick.bCanEverTick = true;

    helth_component  = CreateDefaultSubobject<USTU_Helth_Component>("helth_component");
    weapon_component = CreateDefaultSubobject<USTU_Weapon_Component>("weapon_component");
}

// Called when the game starts or when spawned
void ASTU_Base_Character::BeginPlay()
{
    Super::BeginPlay();

    check(helth_component);
    check(GetCharacterMovement());
    check(GetMesh());

    on_helth_change(helth_component->get_helth(), 0.0f);
    helth_component->on_death.AddUObject(this, &ASTU_Base_Character::on_death);
    helth_component->on_helth_change.AddUObject(this, &ASTU_Base_Character::on_helth_change);

    LandedDelegate.AddDynamic(this, &ASTU_Base_Character::on_ground_landed);
}

// Called every frame
void ASTU_Base_Character::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

float ASTU_Base_Character::get_movement_direction() const
{
    if (GetVelocity().IsZero())
        return 0.0f;

    const auto velocity_normal = GetVelocity().GetSafeNormal();
    const auto angle_between =
        FMath::Acos(FVector::DotProduct(GetActorForwardVector(), velocity_normal));
    const auto cross_product = FVector::CrossProduct(GetActorForwardVector(), velocity_normal);
    const auto degrees       = FMath::RadiansToDegrees(angle_between);
    return cross_product.IsZero() ? degrees : degrees * FMath::Sign(cross_product.Z);
}

bool ASTU_Base_Character::is_running() const
{
    return false;
}

void ASTU_Base_Character::on_death()
{
    // PlayAnimMontage(death_anim_montage);
    GetCharacterMovement()->DisableMovement();
    SetLifeSpan(dead_span);

    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    weapon_component->stop_fire();
    weapon_component->zoom(false);

    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);

    UGameplayStatics::PlaySoundAtLocation(GetWorld(), death_sound, GetActorLocation());
}

void ASTU_Base_Character::on_helth_change(float hp, float delta) {}

void ASTU_Base_Character::on_ground_landed(const FHitResult& hit)
{
    const auto fall_velocity_z = GetVelocity().Z;
    UE_LOG(base_character_log, Display, TEXT("On landed: %f"), fall_velocity_z);

    if (fall_velocity_z < landed_damage_velocity.X)
        return;

    const auto final_damage =
        FMath::GetMappedRangeValueClamped(landed_damage_velocity, landed_damage, fall_velocity_z);

    TakeDamage(final_damage, FDamageEvent(), nullptr, nullptr);
}

void ASTU_Base_Character::set_player_color(const FLinearColor& color)
{
    const auto material_instance = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
    if (!material_instance)
        return;

    material_instance->SetVectorParameterValue(material_color_name, color);
}

void ASTU_Base_Character::TurnOff()
{
    weapon_component->stop_fire();
    weapon_component->zoom(false);
    Super::TurnOff();
}
void ASTU_Base_Character::Reset()
{
    weapon_component->stop_fire();
    weapon_component->zoom(false);
    Super::Reset();
}