// Shoot_Them_Up game from Ckopnuk

#include "Animations/STU_AnimNotify.h"

void USTU_AnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    on_natified.Broadcast(MeshComp);
    Super::Notify(MeshComp, Animation);
}