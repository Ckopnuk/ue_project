// Shoot_Them_Up game from Ckopnuk

#include "Component/STU_AI_Perception_Component.h"
#include "AIController.h"
#include "Component/STU_Helth_Component.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Sight.h"
#include "STU_Utils.h"

AActor* USTU_AI_Perception_Component::get_closest_enemy() const
{
    TArray<AActor*> percieve_actors;
    GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), percieve_actors);
    if (percieve_actors.Num() == 0)
    {
        GetCurrentlyPerceivedActors(UAISense_Damage::StaticClass(), percieve_actors);
        if (percieve_actors.Num() == 0)
            return nullptr;
    }

    const auto controller = Cast<AAIController>(GetOwner());
    if (!controller)
        return nullptr;

    const auto pawn = controller->GetPawn();
    if (!pawn)
        return nullptr;

    float   best_distance = MAX_FLT;
    AActor* best_pawn     = nullptr;

    for (const auto percieve_actor : percieve_actors)
    {
        const auto health_component =
            STU_Utils::get_STU_player_component<USTU_Helth_Component>(percieve_actor);

        const auto percieve_pawn = Cast<APawn>(percieve_actor);
        const auto are_enemies =
            percieve_pawn && STU_Utils::are_enemies(controller, percieve_pawn->Controller);

        if (health_component && !health_component->is_dead() && are_enemies)
        {
            const auto current_distance =
                (percieve_actor->GetActorLocation() - pawn->GetActorLocation()).Size();
            if (current_distance < best_distance)
            {
                best_distance = current_distance;
                best_pawn     = percieve_actor;
            }
        }
    }
    return best_pawn;
}