// Shoot_Them_Up game from Ckopnuk

#include "Component/STU_Helth_Component.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Perception/AISense_Damage.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "STU_GameModeBase.h"
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(Helth_Component_Log, All, All)

// Sets default values for this component's properties
USTU_Helth_Component::USTU_Helth_Component()
{
    PrimaryComponentTick.bCanEverTick = false;
}

bool USTU_Helth_Component::try_to_add_health(float health_amount)
{
    if (is_health_full() || is_dead())
    {
        return false;
    }
    else
    {
        set_health(health + health_amount);
    }
    return true;
}

bool USTU_Helth_Component::is_health_full()
{
    return FMath::IsNearlyEqual(health, max_health);
}

// Called when the game starts
void USTU_Helth_Component::BeginPlay()
{
    Super::BeginPlay();

    check(max_health > 0)

        set_health(max_health);

    AActor* component_owner = GetOwner();
    if (component_owner)
    {
        component_owner->OnTakeAnyDamage.AddDynamic(
            this, &USTU_Helth_Component::on_take_any_damage_handle);
        component_owner->OnTakePointDamage.AddDynamic(this,
                                                      &USTU_Helth_Component::on_take_point_damage);
        component_owner->OnTakeRadialDamage.AddDynamic(
            this, &USTU_Helth_Component::on_take_radial_damage);
    }
}

void USTU_Helth_Component::on_take_any_damage_handle(AActor* damaged_actor, float damage,
                                                     const class UDamageType* damage_type,
                                                     class AController*       instigated_by,
                                                     AActor*                  damage_causer)
{
    UE_LOG(Helth_Component_Log, Display, TEXT("On any damage: %f"), damage);
}

void USTU_Helth_Component::on_take_point_damage(AActor* DamagedActor, float Damage,
                                                AController* InstigatedBy, FVector HitLocation,
                                                UPrimitiveComponent* FHitComponent, FName BoneName,
                                                FVector            ShotFromDirection,
                                                const UDamageType* DamageType, AActor* DamageCauser)
{
    const auto final_damage = Damage * get_point_damage_modifier(DamagedActor, BoneName);
    UE_LOG(Helth_Component_Log, Display, TEXT("On point damage: %f, final damage: %f, bone: %s"),
           Damage, final_damage, *BoneName.ToString());
    apply_damage(final_damage, InstigatedBy);
}

void USTU_Helth_Component::on_take_radial_damage(AActor* DamagedActor, float Damage,
                                                 const UDamageType* DamageType, FVector Origin,
                                                 FHitResult HitInfo, AController* InstigatedBy,
                                                 AActor* DamageCauser)
{
    UE_LOG(Helth_Component_Log, Display, TEXT("On radial damage: %f"), Damage);
    apply_damage(Damage, InstigatedBy);
}

void USTU_Helth_Component::heal_update()
{
    set_health(health + heal_modifier);

    if (is_health_full() && GetWorld())
    {
        GetWorld()->GetTimerManager().ClearTimer(heal_timer_hendle);
    }
}

void USTU_Helth_Component::set_health(float hp)
{
    const auto next_health  = FMath::Clamp(hp, 0.0f, max_health);
    const auto health_delta = next_health - health;

    health = next_health;
    on_helth_change.Broadcast(health, health_delta);
}

void USTU_Helth_Component::play_camera_shake()
{
    if (is_dead())
        return;

    const auto player = Cast<APawn>(GetOwner());
    if (!player)
        return;

    const auto controller = player->GetController<APlayerController>();
    if (!controller || !controller->PlayerCameraManager)
        return;

    controller->PlayerCameraManager->StartCameraShake(camera_shake);
}

void USTU_Helth_Component::killed(AController* killer_controller)
{
    if (!GetWorld())
        return;

    const auto game_mode = Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode());
    if (!game_mode)
        return;

    const auto player_pawn       = Cast<APawn>(GetOwner());
    const auto victim_controller = player_pawn ? player_pawn->Controller : nullptr;

    game_mode->killed(killer_controller, victim_controller);
}

void USTU_Helth_Component::apply_damage(float damage, AController* instigated_by)
{
    if (damage <= 0.0f || is_dead() && !GetWorld())
        return;

    set_health(health - damage);
    GetWorld()->GetTimerManager().ClearTimer(heal_timer_hendle);

    if (is_dead())
    {
        killed(instigated_by);
        on_death.Broadcast();
    }
    else if (auto_heal)
    {
        GetWorld()->GetTimerManager().SetTimer(heal_timer_hendle, this,
                                               &USTU_Helth_Component::heal_update, heal_update_time,
                                               true, heal_delay);
    }
    play_camera_shake();
    report_damage_event(damage, instigated_by);
}

float USTU_Helth_Component::get_point_damage_modifier(AActor* damaged_actor, const FName& bone_name)
{
    const auto character = Cast<ACharacter>(damaged_actor);
    if (!character || !character->GetMesh() || !character->GetMesh()->GetBodyInstance(bone_name))
        return 1.0f;
    const auto phys_material =
        character->GetMesh()->GetBodyInstance(bone_name)->GetSimplePhysicalMaterial();

    if (!phys_material || !damage_modifires.Contains(phys_material))
        return 1.0f;

    return damage_modifires[phys_material];
}

void USTU_Helth_Component::report_damage_event(float damage, AController* instigated_by)
{
    if (!instigated_by || !instigated_by->GetPawn() || !GetOwner())
        return;
    UAISense_Damage::ReportDamageEvent(GetWorld(), GetOwner(), instigated_by->GetPawn(), damage,
                                       instigated_by->GetPawn()->GetActorLocation(),
                                       GetOwner()->GetActorLocation());
}
