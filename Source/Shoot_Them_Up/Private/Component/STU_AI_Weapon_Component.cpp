// Shoot_Them_Up game from Ckopnuk

#include "Component/STU_AI_Weapon_Component.h"
#include "Weapon/STU_Base_Weapon.h"

void USTU_AI_Weapon_Component::start_fire()
{
    if (!can_fire())
        return;

    if (current_weapon->is_ammo_empty())
    {
        next_weapon();
    }
    else
    {
        current_weapon->start_fire();
    }
}

void USTU_AI_Weapon_Component::next_weapon()
{
    if (!can_equip())
        return;

    int32 next_index = (current_weapon_index + 1) % weapons.Num();
    while (next_index != current_weapon_index)
    {
        if (!weapons[next_index]->is_ammo_empty())
            break;
        next_index = (next_index + 1) % weapons.Num();
    }

    if (current_weapon_index != next_index)
    {
        current_weapon_index = next_index;
        equip_weapon(current_weapon_index);
    }
}