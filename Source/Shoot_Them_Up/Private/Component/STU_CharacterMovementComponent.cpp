// Shoot_Them_Up game from Ckopnuk


#include "Component/STU_CharacterMovementComponent.h"
#include "Player/STU_Base_Character.h"

float USTU_CharacterMovementComponent::GetMaxSpeed() const {
    const float max_speed = Super::GetMaxSpeed();
    const ASTU_Base_Character* player =
        Cast<ASTU_Base_Character>(GetPawnOwner());
    return player && player->is_running() ? max_speed * run_modifier
                                          : max_speed;
}