// Shoot_Them_Up game from Ckopnuk

#include "Component/STU_Respawn_Component.h"
#include "STU_GameModeBase.h"

USTU_Respawn_Component::USTU_Respawn_Component()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void USTU_Respawn_Component::respawn(int32 respawn_time)
{
    if (!GetWorld())
        return;

    respawn_count_down = respawn_time;
    GetWorld()->GetTimerManager().SetTimer(
        respawn_timer_handle, this, &USTU_Respawn_Component::respawn_timer_update, 1.0, true);
}

bool USTU_Respawn_Component::is_respawn_in_progress() const
{
    return GetWorld() && GetWorld()->GetTimerManager().IsTimerActive(respawn_timer_handle);
}

void USTU_Respawn_Component::respawn_timer_update()
{
    if (--respawn_count_down == 0)
    {
        if (!GetWorld())
            return;
        GetWorld()->GetTimerManager().ClearTimer(respawn_timer_handle);

        const auto game_mode = Cast<ASTU_GameModeBase>(GetWorld()->GetAuthGameMode());
        if (!game_mode)
            return;

        game_mode->respawn_reques(Cast<AController>(GetOwner()));
    }
}
