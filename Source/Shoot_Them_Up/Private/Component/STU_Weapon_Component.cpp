// Shoot_Them_Up game from Ckopnuk

#include "Component/STU_Weapon_Component.h"
#include "Animations/Ainm_Utils.h"
#include "Animations/STU_Equip_Finish_Anim_Notify.h"
#include "Animations/STU_Reload_Finish_AnimNotify.h"
#include "GameFramework/Character.h"
#include "Weapon/STU_Base_Weapon.h"

DEFINE_LOG_CATEGORY_STATIC(weapon_component_log, All, All)

constexpr static int32 weapon_num = 2;

USTU_Weapon_Component::USTU_Weapon_Component()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void USTU_Weapon_Component::BeginPlay()
{
    Super::BeginPlay();

    checkf(weapon_data.Num() == weapon_num, TEXT("Our character can hold only %i weapon items"),
           weapon_num);

    current_weapon_index = 0;
    init_animation();
    spawn_weapons();
    equip_weapon(current_weapon_index);
}

void USTU_Weapon_Component::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    current_weapon = nullptr;
    for (auto weapon : weapons)
    {
        weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
        weapon->Destroy();
    }
    weapons.Empty();

    Super::EndPlay(EndPlayReason);
}

void USTU_Weapon_Component::spawn_weapons()
{
    ACharacter* character = Cast<ACharacter>(GetOwner());
    if (!character || !GetWorld())
        return;

    for (auto one_weapon_data : weapon_data)
    {
        auto weapon = GetWorld()->SpawnActor<ASTU_Base_Weapon>(one_weapon_data.weapon_class);
        if (!weapon)
            continue;

        weapon->on_clip_empty.AddUObject(this, &USTU_Weapon_Component::on_empty_clip);
        weapon->SetOwner(character);
        weapons.Add(weapon);

        attach_weapon_to_socket(weapon, character->GetMesh(), weapon_armory_socket_name);
    }
}

void USTU_Weapon_Component::attach_weapon_to_socket(ASTU_Base_Weapon* weapon,
                                                    USceneComponent*  scene_component,
                                                    const FName&      socket_name)
{
    if (!weapon || !scene_component)
        return;
    FAttachmentTransformRules attachment_rules(EAttachmentRule::SnapToTarget, false);

    weapon->AttachToComponent(scene_component, attachment_rules, socket_name);
}

void USTU_Weapon_Component::equip_weapon(int32 weapon_index)
{
    if (weapon_index < 0 || weapon_index >= weapons.Num())
    {
        UE_LOG(weapon_component_log, Warning, TEXT("invalid weapon index"));
        return;
    }
    ACharacter* character = Cast<ACharacter>(GetOwner());
    if (!character)
        return;

    if (current_weapon)
    {
        current_weapon->zoom(false);
        current_weapon->stop_fire();
        attach_weapon_to_socket(current_weapon, character->GetMesh(), weapon_armory_socket_name);
    }

    current_weapon = weapons[weapon_index];
    // current_reload_AM = weapon_data[weapon_index].reload_anim_montage;

    const auto current_weapon_data = weapon_data.FindByPredicate(
        [&](const FWeapon_Data& data_weapon)
        { return data_weapon.weapon_class == current_weapon->GetClass(); });

    current_reload_AM = current_weapon_data ? current_weapon_data->reload_anim_montage : nullptr;

    attach_weapon_to_socket(current_weapon, character->GetMesh(), weapon_equip_socket_name);
    equip_anim_in_progress = true;
    play_anim_montage(equip_anim_montage);
}

void USTU_Weapon_Component::start_fire()
{
    if (!can_fire())
        return;
    current_weapon->start_fire();
}

void USTU_Weapon_Component::stop_fire()
{

    if (!current_weapon)
        return;
    current_weapon->stop_fire();
}

void USTU_Weapon_Component::next_weapon()
{
    if (!can_equip())
        return;

    current_weapon_index = (current_weapon_index + 1) % weapons.Num();
    equip_weapon(current_weapon_index);
}

void USTU_Weapon_Component::reload()
{
    change_clip();
}

bool USTU_Weapon_Component::get_current_weapon_UI_data(FWeapon_UI_Data& ui_data) const
{
    if (current_weapon)
    {
        ui_data = current_weapon->get_UI_data();
        return true;
    }
    return false;
}

bool USTU_Weapon_Component::get_current_weapon_ammo_data(FAmmo_Data& ammo_data) const
{
    if (current_weapon)
    {
        ammo_data = current_weapon->get_ammo_data();
        return true;
    }
    return false;
}

bool USTU_Weapon_Component::try_to_add_ammo(TSubclassOf<ASTU_Base_Weapon> weapon_type,
                                            int32                         clips_amount)
{
    for (const auto weapon : weapons)
    {
        if (weapon && weapon->IsA(weapon_type))
        {
            return weapon->try_to_add_ammo(clips_amount);
        }
    }
    return false;
}

bool USTU_Weapon_Component::need_ammo_to_reload(TSubclassOf<ASTU_Base_Weapon> weapon_type)
{
    for (const auto weapon : weapons)
    {
        if (weapon && weapon->IsA(weapon_type))
        {
            return !weapon->is_ammo_full();
        }
    }
    return false;
}

void USTU_Weapon_Component::zoom(bool enabled)
{
    if (current_weapon)
    {

        current_weapon->zoom(enabled);
    }
}

void USTU_Weapon_Component::play_anim_montage(UAnimMontage* animation)
{
    ACharacter* character = Cast<ACharacter>(GetOwner());
    if (!character)
        return;

    character->PlayAnimMontage(animation);
}

void USTU_Weapon_Component::init_animation()
{
    auto equip_finished_notify =
        Anim_Utils::find_notify_by_class<USTU_Equip_Finish_Anim_Notify>(equip_anim_montage);
    if (equip_finished_notify)
    {
        equip_finished_notify->on_natified.AddUObject(this,
                                                      &USTU_Weapon_Component::on_equip_finish);
    }
    else
    {
        UE_LOG(weapon_component_log, Error, TEXT("Equip anim notify is forgoten to set!"));
        checkNoEntry();
    }

    for (auto one_weapon_data : weapon_data)
    {
        auto reload_finished_notify =
            Anim_Utils::find_notify_by_class<USTU_Reload_Finish_AnimNotify>(
                one_weapon_data.reload_anim_montage);
        if (!reload_finished_notify)
        {
            UE_LOG(weapon_component_log, Error, TEXT("Reload anim notify is forgoten to set!"));
            checkNoEntry();
        }

        reload_finished_notify->on_natified.AddUObject(this,
                                                       &USTU_Weapon_Component::on_reload_finish);
    }
}

void USTU_Weapon_Component::on_equip_finish(USkeletalMeshComponent* mesh_component)
{
    ACharacter* character = Cast<ACharacter>(GetOwner());
    if (!character || character->GetMesh() != mesh_component)
        return;

    equip_anim_in_progress = false;
}

void USTU_Weapon_Component::on_reload_finish(USkeletalMeshComponent* mesh_component)
{
    ACharacter* character = Cast<ACharacter>(GetOwner());
    if (!character || character->GetMesh() != mesh_component)
        return;

    reload_anim_in_progress = false;
}

bool USTU_Weapon_Component::can_fire()
{
    return current_weapon && !equip_anim_in_progress && !reload_anim_in_progress;
}

bool USTU_Weapon_Component::can_equip()
{
    return !equip_anim_in_progress && !reload_anim_in_progress;
}

bool USTU_Weapon_Component::can_reload()
{
    return current_weapon && !equip_anim_in_progress && !reload_anim_in_progress &&
           current_weapon->can_reload();
    return false;
}

void USTU_Weapon_Component::on_empty_clip(ASTU_Base_Weapon* weapon_to_reload)
{
    if (!weapon_to_reload)
        return;

    if (current_weapon == weapon_to_reload)
    {
        change_clip();
    }
    else
    {
        for (const auto weapon : weapons)
        {
            if (weapon == weapon_to_reload)
            {
                weapon->change_clip();
            }
        }
    }
}

void USTU_Weapon_Component::change_clip()
{
    if (!can_reload())
        return;
    current_weapon->stop_fire();
    current_weapon->change_clip();
    reload_anim_in_progress = true;
    play_anim_montage(current_reload_AM);
}
