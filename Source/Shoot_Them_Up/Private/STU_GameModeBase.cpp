// Shoot_Them_Up game from Ckopnuk

#include "STU_GameModeBase.h"
#include "AIController.h"
#include "Component/STU_Respawn_Component.h"
#include "Component/STU_Weapon_Component.h"
#include "EngineUtils.h"
#include "Player/STU_Base_Character.h"
#include "Player/STU_PlayerController.h"
#include "Player/STU_Player_State.h"
#include "STU_Game_Instance.h"
#include "STU_Utils.h"
#include "UI/STU_Game_HUD.h"

DEFINE_LOG_CATEGORY_STATIC(Log_STU_Game_mode_base, All, All);

ASTU_GameModeBase::ASTU_GameModeBase()
{
    DefaultPawnClass      = ASTU_Base_Character::StaticClass();
    PlayerControllerClass = ASTU_PlayerController::StaticClass();
    HUDClass              = ASTU_Game_HUD::StaticClass();
    PlayerStateClass      = ASTU_Player_State::StaticClass();
}

void ASTU_GameModeBase::StartPlay()
{
    Super::StartPlay();

    spawn_bots();
    create_teams_info();
    current_round = 1;

    start_round();

    set_match_state(ESTU_Match_State::im_progress);
}
void ASTU_GameModeBase::spawn_bots()
{
    if (!GetWorld())
        return;

    for (int32 i = 0; i < game_data.players_num - 1; ++i)
    {
        FActorSpawnParameters spawn_info;
        spawn_info.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        const auto stu_ai_controller =
            GetWorld()->SpawnActor<AAIController>(ai_controller_class, spawn_info);
        RestartPlayer(stu_ai_controller);
    }
}

UClass* ASTU_GameModeBase::GetDefaultPawnClassForController_Implementation(
    AController* in_controller)
{
    if (in_controller && in_controller->IsA<AAIController>())
    {
        return ai_pawn_class;
    }
    return Super::GetDefaultPawnClassForController_Implementation(in_controller);
}

void ASTU_GameModeBase::start_round()
{
    round_count_down = game_data.round_time;
    GetWorldTimerManager().SetTimer(game_round_timer_handle, this,
                                    &ASTU_GameModeBase::game_timer_update, 1.0f, true);
}
void ASTU_GameModeBase::game_timer_update()
{
    // UE_LOG(Log_STU_Game_mode_base, Display, TEXT("Time: %i / Round: %i/%i"), round_count_down,
    //       current_round, game_data.rounds_num);

    // const auto timer_rate = GetWorldTimerManager().GetTimerRate(game_round_timer_handle);
    // round_count_down -= timer_rate;

    if (--round_count_down == 0)
    {
        GetWorldTimerManager().ClearTimer(game_round_timer_handle);

        if (current_round + 1 <= game_data.rounds_num)
        {
            ++current_round;
            reset_players();
            start_round();
        }
        else
        {
            geme_over();
        }
    }
}

void ASTU_GameModeBase::reset_players()
{
    if (!GetWorld())
        return;

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        reset_one_player(It->Get());
    }
}

void ASTU_GameModeBase::reset_one_player(AController* controller)
{
    if (controller && controller->GetPawn())
    {
        controller->GetPawn()->Reset();
    }

    RestartPlayer(controller);
    set_player_color(controller);
}

void ASTU_GameModeBase::create_teams_info()
{
    if (!GetWorld())
        return;

    int32 team_id = 1;
    for (auto it = GetWorld()->GetControllerIterator(); it; ++it)
    {
        const auto controller = it->Get();
        if (!controller)
            continue;

        const auto player_state = Cast<ASTU_Player_State>(controller->PlayerState);
        if (!player_state)
            continue;

        player_state->set_team_id(team_id);
        player_state->set_team_color(determine_color_by_team_id(team_id));
        player_state->SetPlayerName(controller->IsPlayerController() ? "player" : "bot");
        set_player_color(controller);

        team_id = team_id == 1 ? 2 : 1;
    }
}

FLinearColor ASTU_GameModeBase::determine_color_by_team_id(int32 team) const
{
    if (team - 1 < game_data.team_colors.Num())
    {
        return game_data.team_colors[team - 1];
    }
    UE_LOG(Log_STU_Game_mode_base, Warning, TEXT("No color for team id: %i, set to default: %s"),
           team, *game_data.default_team_color.ToString());
    return game_data.default_team_color;
}

void ASTU_GameModeBase::set_player_color(AController* controller)
{
    if (!controller)
        return;
    const auto chartacter = Cast<ASTU_Base_Character>(controller->GetPawn());
    if (!chartacter)
        return;

    const auto player_state = Cast<ASTU_Player_State>(controller->PlayerState);
    if (!player_state)
        return;

    chartacter->set_player_color(player_state->get_team_color());
}

void ASTU_GameModeBase::killed(AController* killer_controller, AController* victim_controller)
{
    const auto killer_player_state =
        killer_controller ? Cast<ASTU_Player_State>(killer_controller->PlayerState) : nullptr;
    const auto victim_player_state =
        victim_controller ? Cast<ASTU_Player_State>(victim_controller->PlayerState) : nullptr;

    if (killer_player_state)
    {
        killer_player_state->add_kill();
    }

    if (victim_player_state)
    {
        victim_player_state->add_death();
    }

    start_respawm(victim_controller);
}

void ASTU_GameModeBase::log_player_info()
{
    if (!GetWorld())
        return;

    for (auto it = GetWorld()->GetControllerIterator(); it; ++it)
    {
        const auto controller = it->Get();
        if (!controller)
            continue;

        const auto player_state = Cast<ASTU_Player_State>(controller->PlayerState);
        if (!player_state)
            continue;

        player_state->log_info();
    }
}

void ASTU_GameModeBase::start_respawm(AController* controller)
{
    const bool respawn_awailable =
        round_count_down > game_data.min_round_time_for_respawn + game_data.respawn_time;
    if (!respawn_awailable)
        return;
    const auto respawn_commponent =
        STU_Utils::get_STU_player_component<USTU_Respawn_Component>(controller);
    if (!respawn_commponent)
        return;

    respawn_commponent->respawn(game_data.respawn_time);
}

void ASTU_GameModeBase::geme_over()
{
    UE_LOG(Log_STU_Game_mode_base, Display, TEXT("=============GAME OVER============"));
    log_player_info();

    for (auto pawn : TActorRange<APawn>(GetWorld()))
    {
        if (pawn)
        {
            pawn->TurnOff();
            pawn->DisableInput(nullptr);
        }
    }
    set_match_state(ESTU_Match_State::game_over);
}

void ASTU_GameModeBase::set_match_state(ESTU_Match_State state)
{
    if (match_state == state)
        return;

    match_state = state;
    on_match_changed.Broadcast(match_state);
}

void ASTU_GameModeBase::stop_all_fire() 
{
    for (auto pawn : TActorRange<APawn>(GetWorld()))
    {
        const auto weapon_component =
            STU_Utils::get_STU_player_component<USTU_Weapon_Component>(pawn);
        if (!weapon_component)
            continue;
        weapon_component->stop_fire();
        weapon_component->zoom(false);
    }
}

int32 ASTU_GameModeBase::get_current_round() const
{
    return current_round;
}

FGame_data ASTU_GameModeBase::get_game_data() const
{
    return game_data;
}

int32 ASTU_GameModeBase::get_round_seconds_remaining() const
{
    return round_count_down;
}

bool ASTU_GameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
    const bool pause_set = Super::SetPause(PC, CanUnpauseDelegate);
    if (pause_set)
    {
        stop_all_fire();
        set_match_state(ESTU_Match_State::pause);
    }

    return pause_set;
}

bool ASTU_GameModeBase::ClearPause()
{
    const bool pause_clear = Super::ClearPause();
    if (pause_clear)
    {
        set_match_state(ESTU_Match_State::im_progress);
    }

    return pause_clear;
}

void ASTU_GameModeBase::respawn_reques(AController* controller)
{
    reset_one_player(controller);
}
