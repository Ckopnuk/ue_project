// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "STU_Sound_Func_Lib.generated.h"

class USoundClass;

UCLASS()
class SHOOT_THEM_UP_API USTU_Sound_Func_Lib : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable)
    static void set_sound_class_volume(USoundClass* sound_class, float volume);
    UFUNCTION(BlueprintCallable)
    static void toggle_sound_class_volume(USoundClass* sound_class);
};
