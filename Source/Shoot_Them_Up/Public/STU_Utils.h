#pragma once
#include "Player/STU_Player_State.h"

class STU_Utils
{
public:
    template <typename T>
    static T* get_STU_player_component(AActor* player_pawn)
    {
        if (!player_pawn)
            return nullptr;

        const auto component = player_pawn->GetComponentByClass(T::StaticClass());
        return Cast<T>(component);
    };

    bool static are_enemies(AController* controller_1, AController* controller_2)
    {
        if (!controller_1 || !controller_2 || controller_1 == controller_2)
            return false;

        const auto player_state_1 = Cast<ASTU_Player_State>(controller_1->PlayerState);
        const auto player_state_2 = Cast<ASTU_Player_State>(controller_2->PlayerState);

        return player_state_1 && player_state_2 &&
               player_state_1->get_team_id() != player_state_2->get_team_id();
    }

    static FText text_from_int(int32 number) { return FText::FromString(FString::FromInt(number)); }
};