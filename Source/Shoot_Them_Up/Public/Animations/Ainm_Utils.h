#pragma once

class Anim_Utils
{
public:
    template <typename T>
    static T* find_notify_by_class(UAnimSequenceBase* animation)
    {
        if (!animation)
            return nullptr;
        const auto notify_events = animation->Notifies;
        for (auto notify_event : notify_events)
        {
            auto anim_notify = Cast<T>(notify_event.Notify);
            if (anim_notify)
            {
                return anim_notify;
            }
        }
        return nullptr;
    }
};

