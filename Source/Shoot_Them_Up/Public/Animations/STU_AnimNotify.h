// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Animation/AnimNotifies/AnimNotify.h"
#include "CoreMinimal.h"
#include "STU_AnimNotify.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifiedSignature,
                                    USkeletalMeshComponent*)

    UCLASS() class SHOOT_THEM_UP_API USTU_AnimNotify : public UAnimNotify
{
    GENERATED_BODY()

public:
    virtual void Notify(USkeletalMeshComponent* MeshComp,
                        UAnimSequenceBase*      Animation) override;

    FOnNotifiedSignature on_natified;
};
