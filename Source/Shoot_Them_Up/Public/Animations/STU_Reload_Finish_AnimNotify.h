// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Animations/STU_AnimNotify.h"
#include "STU_Reload_Finish_AnimNotify.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Reload_Finish_AnimNotify : public USTU_AnimNotify
{
	GENERATED_BODY()
	
};
