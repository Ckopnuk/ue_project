// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Animations/STU_AnimNotify.h"
#include "CoreMinimal.h"

#include "STU_Equip_Finish_Anim_Notify.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Equip_Finish_Anim_Notify : public USTU_AnimNotify
{
    GENERATED_BODY()
};
