// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Pickups/STU_Base_Pickup.h"

#include "MySTU_Health_Pickup.generated.h"

UCLASS()
class SHOOT_THEM_UP_API AMySTU_Health_Pickup : public ASTU_Base_Pickup
{
    GENERATED_BODY()

        protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup",
              meta = (ClampMin = "5.0", ClampMax = "100.0"))
    float health_amount = 25.0f;

private:
    virtual bool give_pickup_to(APawn* player_pawn) override;
};
