// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Pickups/STU_Base_Pickup.h"
#include "STU_Ammo_Pickup.generated.h"

class ASTU_Base_Weapon;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Ammo_Pickup : public ASTU_Base_Pickup
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup",
              meta = (ClampMin = "1.0", ClampMax = "10.0"))
    int clips_amount = 10;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    TSubclassOf<ASTU_Base_Weapon> weapon_type;

private:
    virtual bool give_pickup_to(APawn* player_pawn) override;
};
