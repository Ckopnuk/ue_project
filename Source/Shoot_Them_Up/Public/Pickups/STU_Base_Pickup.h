// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STU_Base_Pickup.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Base_Pickup : public AActor
{
    GENERATED_BODY()

public:
    ASTU_Base_Pickup();

protected:
    UPROPERTY(VisibleAnywhere, Category = "Pickup")
    USphereComponent* collision_component;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    float respawn_time = 5.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundCue* pickup_taken_sound;

    virtual void BeginPlay() override;
    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:
    virtual void Tick(float DeltaTime) override;
    bool         could_be_taken() const;

private:
    FTimerHandle respawn_timer_handle;
    float        rotation_yaw = 0.0f;

    virtual bool give_pickup_to(APawn* player_pawn);

    void pickup_was_taken();
    void respawn();
    void generate_rotation_yaw();
};
