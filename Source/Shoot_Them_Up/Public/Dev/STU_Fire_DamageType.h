// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "STU_Fire_DamageType.generated.h"

/**
 * 
 */
UCLASS()
class SHOOT_THEM_UP_API USTU_Fire_DamageType : public UDamageType
{
	GENERATED_BODY()
	
};
