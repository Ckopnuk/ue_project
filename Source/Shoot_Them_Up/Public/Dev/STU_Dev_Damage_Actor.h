// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STU_Dev_Damage_Actor.generated.h"

UCLASS()
class SHOOT_THEM_UP_API ASTU_Dev_Damage_Actor : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASTU_Dev_Damage_Actor();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    USceneComponent* scene_component;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float radius = 300;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FColor sphere_color = FColor::Red;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float damage = 10;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool du_full_damage = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<UDamageType> damage_type;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
};
