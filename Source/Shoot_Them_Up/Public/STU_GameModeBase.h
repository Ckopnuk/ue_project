// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "STU_Core_Types.h"

#include "STU_GameModeBase.generated.h"

class AAIController;

UCLASS()
class SHOOT_THEM_UP_API ASTU_GameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ASTU_GameModeBase();

    FOn_Match_Changed_Signature on_match_changed;

    virtual void    StartPlay() override;
    virtual UClass* GetDefaultPawnClassForController_Implementation(
        AController* in_controller) override;

    void killed(AController* killer_controller, AController* victim_controller);

    int32        get_current_round() const;
    FGame_data   get_game_data() const;
    int32        get_round_seconds_remaining() const;
    virtual bool SetPause(APlayerController* PC,
                          FCanUnpause        CanUnpauseDelegate = FCanUnpause()) override;
    bool         ClearPause() override;

    void respawn_reques(AController* controller);

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TSubclassOf<APawn> ai_pawn_class;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TSubclassOf<AAIController> ai_controller_class;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    FGame_data game_data;

private:
    ESTU_Match_State match_state = ESTU_Match_State::waiting_to_start;

    int32        current_round    = 1;
    int32        round_count_down = 0;
    FTimerHandle game_round_timer_handle;

    void spawn_bots();
    void start_round();
    void game_timer_update();

    void reset_players();
    void reset_one_player(AController* controller);

    void         create_teams_info();
    FLinearColor determine_color_by_team_id(int32 team) const;
    void         set_player_color(AController* controller);

    void log_player_info();
    void start_respawm(AController* controller);

    void geme_over();

    void set_match_state(ESTU_Match_State state);

    void stop_all_fire();
};
