// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "STU_Core_Types.h"

#include "STU_Game_Instance.generated.h"

class USoundClass;

UCLASS()
class SHOOT_THEM_UP_API USTU_Game_Instance : public UGameInstance
{
    GENERATED_BODY()

public:
    FLevel_Data get_startup_level() const { return startup_level; }
    void        set_startup_level(const FLevel_Data& data) { startup_level = data; }

    FName get_menu_level_name() const { return menu_level_name; }

    TArray<FLevel_Data> get_levels_data() const { return levels_data; }

    void toggle_volume();

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TArray<FLevel_Data> levels_data;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    FName menu_level_name = NAME_None;

    UPROPERTY(EditDefaultsOnly, Category = "Sound")
    USoundClass* master_sound_class;

private:
    FLevel_Data startup_level;
};
