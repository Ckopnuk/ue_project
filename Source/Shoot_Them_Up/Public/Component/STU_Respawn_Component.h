// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "STU_Respawn_Component.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOT_THEM_UP_API USTU_Respawn_Component : public UActorComponent
{
    GENERATED_BODY()
public:
    USTU_Respawn_Component();

    void  respawn(int32 respawn_time);
    int32 get_respawn_count_doun() const { return respawn_count_down; }
    bool  is_respawn_in_progress() const;

private:
    FTimerHandle respawn_timer_handle;
    int32        respawn_count_down = 0;

    void respawn_timer_update();
};
