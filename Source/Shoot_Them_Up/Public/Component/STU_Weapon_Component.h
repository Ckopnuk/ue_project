// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "STU_Core_Types.h"

#include "STU_Weapon_Component.generated.h"

class ASTU_Base_Weapon;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOT_THEM_UP_API USTU_Weapon_Component : public UActorComponent
{
    GENERATED_BODY()

public:
    USTU_Weapon_Component();

    virtual void start_fire();
    virtual void next_weapon();

    void stop_fire();
    void reload();

    bool get_current_weapon_UI_data(FWeapon_UI_Data& ui_data) const;
    bool get_current_weapon_ammo_data(FAmmo_Data& ammo_data) const;

    bool try_to_add_ammo(TSubclassOf<ASTU_Base_Weapon> weapon_type, int32 clips_amount);
    bool need_ammo_to_reload(TSubclassOf<ASTU_Base_Weapon> weapon_type);

    void zoom(bool enabled);

protected:
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
    bool         can_fire();
    bool         can_equip();
    void         equip_weapon(int32 weapon_index);

    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    TArray<FWeapon_Data> weapon_data;
    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    FName weapon_equip_socket_name = "WeaponSocket";
    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    FName weapon_armory_socket_name = "armory_socket";
    UPROPERTY(EditDefaultsOnly, Category = "Animation")
    UAnimMontage* equip_anim_montage;
    UPROPERTY()
    ASTU_Base_Weapon* current_weapon = nullptr;
    UPROPERTY()
    TArray<ASTU_Base_Weapon*> weapons;

    int32 current_weapon_index = 0;

private:
    UPROPERTY()
    UAnimMontage* current_reload_AM = nullptr;

    bool equip_anim_in_progress  = false;
    bool reload_anim_in_progress = false;

    void attach_weapon_to_socket(ASTU_Base_Weapon* weapon, USceneComponent* scene_component,
                                 const FName& socket_name);
    void spawn_weapons();
    void play_anim_montage(UAnimMontage* animation);
    void init_animation();
    void on_equip_finish(USkeletalMeshComponent* mesh_component);
    void on_reload_finish(USkeletalMeshComponent* mesh_component);
    bool can_reload();
    void on_empty_clip(ASTU_Base_Weapon* weapon_to_reload);
    void change_clip();
};
