// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Component/STU_Weapon_Component.h"
#include "CoreMinimal.h"
#include "STU_AI_Weapon_Component.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_AI_Weapon_Component : public USTU_Weapon_Component
{
    GENERATED_BODY()

public:
    virtual void start_fire() override;
    virtual void next_weapon() override;
};
