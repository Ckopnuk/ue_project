// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "STU_CharacterMovementComponent.generated.h"

/**
 *
 */
UCLASS()
class SHOOT_THEM_UP_API USTU_CharacterMovementComponent
    : public UCharacterMovementComponent
{
    GENERATED_BODY()

public:
    virtual float GetMaxSpeed() const override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement",
              meta = (ClampMin = "1.5", ClampMax = "10"))
    float run_modifier = 2.0f;
};
