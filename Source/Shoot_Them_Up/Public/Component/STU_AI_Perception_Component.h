// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "STU_AI_Perception_Component.generated.h"


UCLASS()
class SHOOT_THEM_UP_API USTU_AI_Perception_Component : public UAIPerceptionComponent
{
    GENERATED_BODY()

public:
    AActor* get_closest_enemy() const;
};
