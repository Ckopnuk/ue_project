// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "STU_Core_Types.h"

#include "STU_Helth_Component.generated.h"

class UCameraShakeBase;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOT_THEM_UP_API USTU_Helth_Component : public UActorComponent
{
    GENERATED_BODY()

public:
    USTU_Helth_Component();

    float get_helth() const { return health; }

    UFUNCTION(BlueprintCallable, Category = "Helth")
    bool is_dead() { return FMath::IsNearlyZero(health); }
    UFUNCTION(BlueprintCallable, Category = "Helth")
    float get_helth_percent() const { return health / max_health; }

    bool try_to_add_health(float health_amount);
    bool is_health_full();

    FOn_Death        on_death;
    FOn_Helth_Change on_helth_change;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth",
              meta = (ClampMin = "1", ClampMax = "200"))
    float max_health = 100.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth")
    bool auto_heal = true;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth",
              meta = (EditCondition = "auto_heal"))
    float heal_update_time = 0.3f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth",
              meta = (EditCondition = "auto_heal"))
    float heal_delay = 3.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth",
              meta = (EditCondition = "auto_heal"))
    float heal_modifier = 1.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    TSubclassOf<UCameraShakeBase> camera_shake;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Helth")
    TMap<UPhysicalMaterial*, float> damage_modifires;

private:
    float        health = 0.0f;
    FTimerHandle heal_timer_hendle;

    UFUNCTION()
    void on_take_any_damage_handle(AActor* damaged_actor, float damage,
                                   const class UDamageType* damage_type,
                                   class AController* instigated_by, AActor* damage_causer);

    UFUNCTION()
    void on_take_point_damage(AActor* DamagedActor, float Damage, class AController* InstigatedBy,
                              FVector HitLocation, class UPrimitiveComponent* FHitComponent,
                              FName BoneName, FVector ShotFromDirection,
                              const class UDamageType* DamageType, AActor* DamageCauser);
    UFUNCTION()
    void on_take_radial_damage(AActor* DamagedActor, float Damage,
                               const class UDamageType* DamageType, FVector Origin,
                               FHitResult HitInfo, class AController* InstigatedBy,
                               AActor* DamageCauser);

    void heal_update();
    void set_health(float hp);
    void play_camera_shake();

    void  killed(AController* killer_controller);
    void  apply_damage(float damage, AController* instigated_by);
    float get_point_damage_modifier(AActor* damaged_actor, const FName& bone_name);

    void report_damage_event(float damage, AController* instigated_by);
};
