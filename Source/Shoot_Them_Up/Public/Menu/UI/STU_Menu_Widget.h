// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "STU_Core_Types.h"
#include "UI/STU_Base_Widget.h"

#include "STU_Menu_Widget.generated.h"

class UButton;
class UHorizontalBox;
class USTU_Game_Instance;
class USTU_Level_Item_Widget;
class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API USTU_Menu_Widget : public USTU_Base_Widget
{
    GENERATED_BODY()

protected:
    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation);

    UPROPERTY(meta = (BindWidget))
    UButton* quit_game_button;

    UPROPERTY(meta = (BindWidget))
    UButton* start_game_button;

    UPROPERTY(meta = (BindWidget))
    UHorizontalBox* level_items_box;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* start_game_sound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> level_item_widget_class;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* hide_animation;

private:
    UPROPERTY()
    TArray<USTU_Level_Item_Widget*> level_item_widgets;

    UFUNCTION()
    void on_quit_game();

    UFUNCTION()
    void on_start_game();

    void                init_level_items();
    void                on_level_selected(const FLevel_Data& data);
    USTU_Game_Instance* get_stu_game_instance() const;
};
