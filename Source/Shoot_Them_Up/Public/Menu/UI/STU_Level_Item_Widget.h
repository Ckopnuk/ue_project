// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Core_Types.h"

#include "STU_Level_Item_Widget.generated.h"

class UButton;
class UTextBlock;
class UImage;

UCLASS()
class SHOOT_THEM_UP_API USTU_Level_Item_Widget : public UUserWidget
{
    GENERATED_BODY()

public:
    FOn_On_Level_Selected_Signature on_level_selected;

    void        set_level_data(const FLevel_Data& data);
    FLevel_Data get_level_data() const { return level_data; }

    void set_selected(bool is_selected);

protected:
    UPROPERTY(meta = (BindWidget))
    UTextBlock* level_name_text_block;
    UPROPERTY(meta = (BindWidget))
    UImage* level_image = nullptr;
    UPROPERTY(meta = (BindWidget))
    UImage* frame_image;
    UPROPERTY(meta = (BindWidget))
    UButton* level_select_button;

    virtual void NativeOnInitialized() override;

private:
    FLevel_Data level_data;

    UFUNCTION()
    void on_level_item_clicked();

    UFUNCTION()
    void on_level_item_hovered();
    UFUNCTION()
    void on_level_item_unhovered();
};
