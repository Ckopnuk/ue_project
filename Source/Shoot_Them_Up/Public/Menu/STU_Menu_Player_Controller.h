// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "STU_Menu_Player_Controller.generated.h"

UCLASS()
class SHOOT_THEM_UP_API ASTU_Menu_Player_Controller : public APlayerController
{
    GENERATED_BODY()

public:
    virtual void BeginPlay() override;
};
