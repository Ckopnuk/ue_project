// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "STU_Menu_Game_Mode_Base.generated.h"

UCLASS()
class SHOOT_THEM_UP_API ASTU_Menu_Game_Mode_Base : public AGameModeBase
{
    GENERATED_BODY()

public:
    ASTU_Menu_Game_Mode_Base();
};
