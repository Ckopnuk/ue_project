// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "STU_Menu_HUD.generated.h"

UCLASS()
class SHOOT_THEM_UP_API ASTU_Menu_HUD : public AHUD
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> menu_widget_class;
};
