#pragma once

#include "STU_Core_Types.generated.h"

// weapon

class ASTU_Base_Weapon;

DECLARE_MULTICAST_DELEGATE_OneParam(FOn_Clip_Empty_Signature, ASTU_Base_Weapon*);

USTRUCT(BlueprintType)
struct FAmmo_Data
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    int32 bullets;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon",
              meta = (EditCondition = "!infinite"))
    int32 clips;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    bool infinite;
};

USTRUCT(BlueprintType)
struct FWeapon_Data
{
    GENERATED_USTRUCT_BODY()
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    TSubclassOf<ASTU_Base_Weapon> weapon_class;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    UAnimMontage* reload_anim_montage;
};

USTRUCT(BlueprintType)
struct FWeapon_UI_Data
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    UTexture2D* main_icon;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    UTexture2D* cross_hair_icon;
};

// VFX

class UNiagaraSystem;
class USoundCue;

USTRUCT(BlueprintType)
struct FDacal_Data
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UMaterialInterface* material;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FVector size = FVector(10.0f);

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    float life_time = 5.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    float fade_out_time = 0.7f;
};

USTRUCT(BlueprintType)
struct FImpact_Data
{
    GENERATED_USTRUCT_BODY()
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* niagara_effect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FDacal_Data decal_data;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    USoundCue* sound;
};

// game mode

USTRUCT(BlueprintType) struct FGame_data
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game",
              meta = (ClampMin = "1", ClampMax = "100"))
    int32 players_num = 2;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game",
              meta = (ClampMin = "1", ClampMax = "10"))
    int32 rounds_num = 1;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game",
              meta = (ClampMin = "10", ClampMax = "300"))
    int32 round_time = 10; // in seconds

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
    TArray<FLinearColor> team_colors;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
    FLinearColor default_team_color = FLinearColor::Red;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game",
              meta = (ClampMin = "3", ClampMax = "30"))
    int32 respawn_time = 10; // in seconds

    int min_round_time_for_respawn = 10;
};

// helth

DECLARE_MULTICAST_DELEGATE(FOn_Death)
DECLARE_MULTICAST_DELEGATE_TwoParams(FOn_Helth_Change, float, float)

UENUM(BlueprintType)
enum class ESTU_Match_State : uint8
{
    waiting_to_start = 0,
    im_progress,
    pause,
    game_over
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOn_Match_Changed_Signature, ESTU_Match_State);

USTRUCT(BlueprintType)
struct FLevel_Data
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    FName level_name = "Test_Level";

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    FName level_display_name = NAME_None;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    UTexture2D* level_thumb;
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOn_On_Level_Selected_Signature, const FLevel_Data&);
