// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Go_To_Menu_Widget.generated.h"

class UButton;

UCLASS()
class SHOOT_THEM_UP_API USTU_Go_To_Menu_Widget : public UUserWidget
{
    GENERATED_BODY()

protected:
    virtual void NativeOnInitialized() override;

    UPROPERTY(meta = (BindWidget))
    UButton* go_menu_button;

private:
    UFUNCTION()
    void on_go_to_menu();
};
