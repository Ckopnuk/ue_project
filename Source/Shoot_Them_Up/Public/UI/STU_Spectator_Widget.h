// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "STU_Spectator_Widget.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Spectator_Widget : public UUserWidget
{
	GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Category = "UI")
    bool get_respawn_time(int32& count_down_time) const;
};
