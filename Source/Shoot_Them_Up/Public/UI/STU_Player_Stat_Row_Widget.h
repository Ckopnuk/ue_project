// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Player_Stat_Row_Widget.generated.h"

class UImage;
class UTextBlock;

UCLASS()
class SHOOT_THEM_UP_API USTU_Player_Stat_Row_Widget : public UUserWidget
{
    GENERATED_BODY()

public:
    void set_player_name(const FText& text);
    void set_kills(const FText& text);
    void set_deaths(const FText& text);
    void set_team(const FText& text);
    void set_player_indicator_visibility(bool visible);
    void set_team_color(const FLinearColor& color);

protected:
    UPROPERTY(meta = (BindWidget))
    UTextBlock* player_name_text_block;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* kills_text_block;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* deaths_text_block;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* team_text_block;

    UPROPERTY(meta = (BindWidget))
    UImage* team_image;

    UPROPERTY(meta = (BindWidget))
    UImage* player_indicator_image;
};
