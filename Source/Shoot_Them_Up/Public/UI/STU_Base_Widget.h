// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Base_Widget.generated.h"

class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API USTU_Base_Widget : public UUserWidget
{
    GENERATED_BODY()

public:
    void show();

protected:
    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* show_animation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* open_sound;
};
