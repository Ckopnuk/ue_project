// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "STU_Core_Types.h"
#include "STU_Game_HUD.generated.h"

class USTU_Base_Widget;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Game_HUD : public AHUD
{
    GENERATED_BODY()

public:
    virtual void DrawHUD() override;

private:
    void draw_cross_hair();
    void on_match_state_changed(ESTU_Match_State state);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI");
    TSubclassOf<UUserWidget> player_HUD_widget_class;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI");
    TSubclassOf<UUserWidget> game_over_widget_class;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI");
    TSubclassOf<UUserWidget> pause_widget_class;

    virtual void BeginPlay();

private:
    UPROPERTY()
    TMap<ESTU_Match_State, USTU_Base_Widget*> game_widgets;

    UPROPERTY()
    USTU_Base_Widget* current_widget = nullptr;
};
