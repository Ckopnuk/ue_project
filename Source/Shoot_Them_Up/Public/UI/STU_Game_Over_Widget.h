// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "STU_Core_Types.h"
#include "UI/STU_Base_Widget.h"

#include "STU_Game_Over_Widget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class SHOOT_THEM_UP_API USTU_Game_Over_Widget : public USTU_Base_Widget
{
    GENERATED_BODY()

protected:
    UPROPERTY(meta = (BindWidget))
    UButton* reset_level_button;
    UPROPERTY(meta = (BindWidget))
    UVerticalBox* player_stat_box;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> player_stat_row_widget_class;

    virtual void NativeOnInitialized() override;

private:
    void on_match_state_changed(ESTU_Match_State state);
    void update_player_state();

    UFUNCTION()
    void on_rest_level();
};
