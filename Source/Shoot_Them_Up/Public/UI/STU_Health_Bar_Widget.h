// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Health_Bar_Widget.generated.h"

class UProgressBar;

UCLASS()
class SHOOT_THEM_UP_API USTU_Health_Bar_Widget : public UUserWidget
{
    GENERATED_BODY()

public:
    void set_health_percent(float percent);

protected:
    UPROPERTY(meta = (BindWidget))
    UProgressBar* health_progress_bar;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor good_color = FLinearColor::Green;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor bad_color = FLinearColor::Red;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    float percent_color_threshold = 0.3;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    float percent_visibility_threshold = 0.8;
};
