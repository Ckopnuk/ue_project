// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "UI/STU_Base_Widget.h"
#include "CoreMinimal.h"

#include "STU_Pause_Widget.generated.h"

class UButton;

UCLASS()
class SHOOT_THEM_UP_API USTU_Pause_Widget : public USTU_Base_Widget
{
    GENERATED_BODY()

protected:
    UPROPERTY(meta = (BindWidget))
    UButton* clear_pause_button;

    virtual void NativeOnInitialized() override;

private:
    UFUNCTION()
    void on_clear_pause();
};
