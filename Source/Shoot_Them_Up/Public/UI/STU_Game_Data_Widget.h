// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "STU_Game_Data_Widget.generated.h"

class ASTU_GameModeBase;
class ASTU_Player_State;

UCLASS()
class SHOOT_THEM_UP_API USTU_Game_Data_Widget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, category = "UI")
    int32 get_current_round() const;

    UFUNCTION(BlueprintCallable, category = "UI")
    int32 get_total_rounds_num() const;

    UFUNCTION(BlueprintCallable, category = "UI")
    int32 get_round_seconds_remaining() const;

private:
    ASTU_Player_State* get_stu_player_state() const;
    ASTU_GameModeBase* get_stu_game_mode() const;
};
