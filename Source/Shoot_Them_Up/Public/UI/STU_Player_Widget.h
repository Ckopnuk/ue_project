// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "UI/STU_Base_Widget.h"
#include "CoreMinimal.h"
#include "STU_Core_Types.h"

#include "STU_Player_Widget.generated.h"

class UProgressBar;

UCLASS()
class SHOOT_THEM_UP_API USTU_Player_Widget : public USTU_Base_Widget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category = "UI")
    float get_helth_percent() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool get_current_weapon_UI_data(FWeapon_UI_Data& ui_data) const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool geet_current_weapon_ammo_data(FAmmo_Data& ammo_data) const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool is_player_alive() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool is_player_spectcting() const;

    UFUNCTION(BlueprintImplementableEvent, Category = "UI")
    void on_take_damage();

    UFUNCTION(BlueprintCallable, category = "UI")
    int32 get_kills_num() const;

protected:
    UPROPERTY(meta = (BindWidget))
    UProgressBar* health_progress_bar;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* damage_animation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor good_color = FLinearColor::Green;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor bad_color = FLinearColor::Red;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    float percent_color_threshold = 0.3;

    virtual void NativeOnInitialized() override;

private:
    void on_health_changed(float hp, float delta);
    void on_new_pawn(APawn* new_pawn);
    void update_health_bar();
};
