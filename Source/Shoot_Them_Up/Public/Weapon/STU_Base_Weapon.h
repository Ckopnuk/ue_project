// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STU_Core_Types.h"

#include "STU_Base_Weapon.generated.h"

class USkeletalMeshComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Base_Weapon : public AActor
{
    GENERATED_BODY()

public:
    ASTU_Base_Weapon();

    FOn_Clip_Empty_Signature on_clip_empty;

    virtual void start_fire();
    virtual void stop_fire();
    void         change_clip();
    bool         can_reload() const;
    bool         is_ammo_empty() const;
    bool         is_ammo_full() const;

    virtual void zoom(bool enabled) {}

    FWeapon_UI_Data get_UI_data() const { return UI_data; }
    FAmmo_Data      get_ammo_data() const { return current_ammo; }

    bool try_to_add_ammo(int32 clips_amount);

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USkeletalMeshComponent* weapon_mesh;

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    FName muzzle_socket_name = "Muzzle_Socket";
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    float trace_max_distance = 1500.0f;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    FAmmo_Data default_ammo{ 10, 15, false };

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "UI")
    FWeapon_UI_Data UI_data;

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* muzzle_fx;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* fire_sound;

    virtual void BeginPlay() override;

    virtual void make_shot();
    virtual bool get_trace_data(FVector& trace_start, FVector& trace_end) const;

    FVector get_muzzle_world_location() const;

    bool get_player_view_point(FVector& view_location, FRotator& view_rotation) const;
    void make_hit(FHitResult& hit_result, const FVector& trace_start, const FVector& trace_end);

    void decrease_ammo();
    bool is_clip_empty() const;

    void log_ammo();

    UNiagaraComponent* spawn_muzzle_fx();

private:
    FAmmo_Data current_ammo;
};
