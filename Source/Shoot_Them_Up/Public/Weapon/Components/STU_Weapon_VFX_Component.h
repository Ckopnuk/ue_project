// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "STU_Core_Types.h"

#include "STU_Weapon_VFX_Component.generated.h"

class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOT_THEM_UP_API USTU_Weapon_VFX_Component : public UActorComponent
{
    GENERATED_BODY()

public:
    USTU_Weapon_VFX_Component();

    void play_impact_FX(const FHitResult& hit);

protected:
    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FImpact_Data default_impact_data;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    TMap<UPhysicalMaterial*, FImpact_Data> impact_data_map;

public:
    virtual void TickComponent(float DeltaTime, ELevelTick TickType,
                               FActorComponentTickFunction* ThisTickFunction) override;
};
