// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STU_Base_Weapon.h"

#include "STU_Lauuncher_Weapon.generated.h"

class ASTU_Projectile;
class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Lauuncher_Weapon : public ASTU_Base_Weapon
{
    GENERATED_BODY()

public:
    virtual void start_fire() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    TSubclassOf<ASTU_Projectile> projectile_class;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* no_ammo_sound;

    virtual void make_shot() override;
};
