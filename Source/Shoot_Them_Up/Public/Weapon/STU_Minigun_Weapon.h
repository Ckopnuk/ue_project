// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STU_Base_Weapon.h"
#include "STU_Minigun_Weapon.generated.h"

/**
 * 
 */
UCLASS()
class SHOOT_THEM_UP_API ASTU_Minigun_Weapon : public ASTU_Base_Weapon
{
	GENERATED_BODY()
	
};
