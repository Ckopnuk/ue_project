// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STU_Base_Weapon.h"

#include "STU_Rifle_Weapon.generated.h"

class USTU_Weapon_VFX_Component;
class UNiagaraComponent;
class UNiagaraSystem;
class UAudioComponent;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Rifle_Weapon : public ASTU_Base_Weapon
{
    GENERATED_BODY()

public:
    ASTU_Rifle_Weapon();

    virtual void start_fire() override;
    virtual void stop_fire() override;

    virtual void zoom(bool enabled);

protected:
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    float time_between_shots = 0.1f;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    float bullet_spread = 1.5f;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    float damage_amount = 10.0f;
    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "Weapon")
    float fov_zoom_angle = 50.0f;

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* trace_fx;

    UPROPERTY(EditDefaultsonly, BlueprintReadWrite, Category = "VFX")
    FString trace_target_name = "trace_target";

    UPROPERTY(VisibleAnywhere, Category = "VFX")
    USTU_Weapon_VFX_Component* weapon_fx_component;

    virtual void BeginPlay() override;
    virtual void make_shot() override;
    virtual bool get_trace_data(FVector& trace_start, FVector& trace_end) const override;

private:
    FTimerHandle shot_timer_handle;

    UPROPERTY()
    UNiagaraComponent* muzzle_fx_component;
    UPROPERTY()
    UAudioComponent* fire_audio_component;

    void make_dmg(const FHitResult& hit_result);
    void init_fx();
    void set_fx_active(bool is_active);
    void spawn_trace_fx(const FVector& trace_start, const FVector& trace_end);

    AController* get_controller() const;

    float default_camera_fov = 90.0f;
};