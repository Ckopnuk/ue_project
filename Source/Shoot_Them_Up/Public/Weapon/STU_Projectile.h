// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STU_Projectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class USTU_Weapon_VFX_Component;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Projectile : public AActor
{
    GENERATED_BODY()

public:
    ASTU_Projectile();

    void set_shot_direction(const FVector& dir) { shot_direction = dir; }

protected:
    UPROPERTY(VisibleAnywhere, Category = "Weapon")
    USphereComponent* collision_component;
    UPROPERTY(VisibleAnywhere, Category = "Weapon")
    UProjectileMovementComponent* movement_component;
    UPROPERTY(VisibleAnywhere, Category = "VFX")
    USTU_Weapon_VFX_Component* weapon_fx_component;

    UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float damage_radius = 200.0f;
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float damage_amount = 75.0f;
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    bool du_full_damage = false;
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    float life_seconds = 5.0f;

    virtual void BeginPlay() override;

private:
    FVector shot_direction;

    UFUNCTION()
    void on_projectile_hit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                           UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                           const FHitResult& Hit);

    AController* get_controller() const;
};
