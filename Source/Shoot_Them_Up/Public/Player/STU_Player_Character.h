// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Player/STU_Base_Character.h"

#include "STU_Player_Character.generated.h"

class UCameraComponent;
class USpringArmComponent;
class USphereComponent;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Player_Character : public ASTU_Base_Character
{
    GENERATED_BODY()

public:
    ASTU_Player_Character(const FObjectInitializer& ObInit);

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USpringArmComponent* spring_arm_component;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USphereComponent* camera_collision_component;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UCameraComponent* camera_component;

    virtual void on_death() override;

    virtual void BeginPlay() override;

public:
    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual bool is_running() const override;

private:
    UFUNCTION()
    void on_camera_collision_begin_overlap(UPrimitiveComponent* OverlappedComponent,
                                           AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                           int32 OtherBodyIndex, bool bFromSweep,
                                           const FHitResult& SweepResult);
    UFUNCTION()
    void on_camera_collision_end_overlap(UPrimitiveComponent* OverlappedComponent,
                                         AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                         int32 OtherBodyIndex);

    void check_camera_overlap();

    bool is_move_forward = false;
    bool wants_to_run    = false;

    void move_forward(float amount);
    void move_right(float amount);

    void on_start_run();
    void on_finish_run();
};
