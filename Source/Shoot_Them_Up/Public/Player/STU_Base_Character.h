// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "Engine/EngineTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "STU_Base_Character.generated.h"

class USTU_Helth_Component;
class USTU_Weapon_Component;
class USoundCue;

UCLASS()
class SHOOT_THEM_UP_API ASTU_Base_Character : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ASTU_Base_Character(const FObjectInitializer& ObInit);
    void set_player_color(const FLinearColor& color);

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTU_Helth_Component* helth_component;
   
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTU_Weapon_Component* weapon_component;

    UPROPERTY(EditDefaultsOnly, Category = "Animation")
    UAnimMontage* death_anim_montage;

    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    float dead_span = 5.0f;
    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    FVector2D landed_damage_velocity = FVector2D(900.0f, 2000.0f);
    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    FVector2D landed_damage = FVector2D(10.0f, 100.0f);

    UPROPERTY(EditDefaultsOnly, Category = "Movement")
    float range = 200.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* death_sound;

    UPROPERTY(EditDefaultsOnly, Category = "Mateial")
    FName material_color_name = "Paint Color";

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    virtual void on_death();
    virtual void on_helth_change(float hp, float delta);

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    virtual void TurnOff() override;
    virtual void Reset() override;

    UFUNCTION(BlueprintCallable, Category = "Movement")
    float get_movement_direction() const;

    UFUNCTION(BlueprintCallable, Category = "Movement")
    virtual bool is_running() const;

private:

    

    UFUNCTION()
    void on_ground_landed(const FHitResult& hit);
};
