// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "STU_Core_Types.h"

#include "STU_PlayerController.generated.h"

class USTU_Respawn_Component;

UCLASS()
class SHOOT_THEM_UP_API ASTU_PlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ASTU_PlayerController();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTU_Respawn_Component* respawn_component;

    virtual void BeginPlay() override;
    virtual void OnPossess(APawn* in_pawn) override;
    virtual void SetupInputComponent() override;

private:
    void on_pause_game();
    void on_match_state_changed(ESTU_Match_State state);
    void on_mute_sound();
};
