// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "STU_Player_State.generated.h"

UCLASS()
class SHOOT_THEM_UP_API ASTU_Player_State : public APlayerState
{
    GENERATED_BODY()

public:
    int32        get_team_id() { return team_id; }
    void         set_team_id(int32 id) { team_id = id; }
    FLinearColor get_team_color() { return team_color; }
    void         set_team_color(const FLinearColor& color) { team_color = color; }

    void  add_kill() { ++kills_num; }
    int32 get_kills_num() { return kills_num; }

    void  add_death() { ++deaths_num; }
    int32 get_deaths_nums() { return deaths_num; }

    void log_info();

private:
    int32        team_id;
    FLinearColor team_color;

    int32 kills_num  = 0;
    int32 deaths_num = 0;
};
