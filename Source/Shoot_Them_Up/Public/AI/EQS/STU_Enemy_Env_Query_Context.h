// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "STU_Enemy_Env_Query_Context.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Enemy_Env_Query_Context : public UEnvQueryContext
{
    GENERATED_BODY()

public:
    virtual void ProvideContext(FEnvQueryInstance&    query_instance,
                                FEnvQueryContextData& context_data) const override;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FName enemy_actor_key_name = "enemy_actor";
};
