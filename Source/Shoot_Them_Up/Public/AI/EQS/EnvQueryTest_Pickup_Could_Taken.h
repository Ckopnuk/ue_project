// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"

#include "EnvQueryTest_Pickup_Could_Taken.generated.h"

UCLASS()
class SHOOT_THEM_UP_API UEnvQueryTest_Pickup_Could_Taken : public UEnvQueryTest
{
    GENERATED_BODY()

public:
    UEnvQueryTest_Pickup_Could_Taken(const FObjectInitializer& object_initializer);
    virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
