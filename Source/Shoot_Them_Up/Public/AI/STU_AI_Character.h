// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "CoreMinimal.h"
#include "Player/STU_Base_Character.h"
#include "STU_AI_Character.generated.h"

class UBehaviorTree;
class UWidgetComponent;

UCLASS()
class SHOOT_THEM_UP_API ASTU_AI_Character : public ASTU_Base_Character
{
    GENERATED_BODY()

public:
    ASTU_AI_Character(const FObjectInitializer& ObjInit);

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
    UBehaviorTree* behavior_tree_asset;

    virtual void Tick(float delta_time) override;

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    float health_visibility_distance = 1000.0f;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UWidgetComponent* health_widget_component;

    virtual void BeginPlay() override;
    virtual void on_helth_change(float hp, float delta) override;
    virtual void on_death() override;

    private:
    void update_health_widget_wisibility();
};
