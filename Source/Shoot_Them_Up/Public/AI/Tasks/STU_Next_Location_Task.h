// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "CoreMinimal.h"
#include "STU_Next_Location_Task.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Next_Location_Task : public UBTTaskNode
{
    GENERATED_BODY()
public:
    USTU_Next_Location_Task();
    virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp,
                                            uint8*                  NodeMemory) override;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    float radius = 1000.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FBlackboardKeySelector aim_location_key;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    bool self_center = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (EditCondition = "!self_center"))
    FBlackboardKeySelector center_actor_key;
};
