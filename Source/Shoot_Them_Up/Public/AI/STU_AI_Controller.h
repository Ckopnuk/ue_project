// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "AIController.h"
#include "CoreMinimal.h"
#include "STU_AI_Controller.generated.h"

class USTU_AI_Perception_Component;
class USTU_Respawn_Component;

UCLASS()
class SHOOT_THEM_UP_API ASTU_AI_Controller : public AAIController
{
    GENERATED_BODY()

public:
    ASTU_AI_Controller();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTU_Respawn_Component* respawn_component;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    USTU_AI_Perception_Component* stu_ai_perception_component;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FName focus_on_key_name = "enemy_actor";

    virtual void OnPossess(APawn* InPawn) override;
    virtual void Tick(float delta_time) override;

private:
    AActor* get_focus_on_actor() const;
};
