// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "BehaviorTree/BTService.h"
#include "CoreMinimal.h"
#include "STU_AI_Change_Weapon_Service.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_AI_Change_Weapon_Service : public UBTService
{
    GENERATED_BODY()
public:
    USTU_AI_Change_Weapon_Service();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI",
              meta = (Clampmin = "0.0", ClampMax = "1.0"))
    float        probability = 0.5f;
    virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory,
                          float DeltaSeconds) override;
};
