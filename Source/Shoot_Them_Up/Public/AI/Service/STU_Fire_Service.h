// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "BehaviorTree/BTService.h"
#include "CoreMinimal.h"
#include "STU_Fire_Service.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Fire_Service : public UBTService
{
    GENERATED_BODY()

public:
    USTU_Fire_Service();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FBlackboardKeySelector enemy_actor_key;

    virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory,
                          float DeltaSeconds) override;
};
