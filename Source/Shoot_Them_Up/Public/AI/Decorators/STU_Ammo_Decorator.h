// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "BehaviorTree/BTDecorator.h"
#include "CoreMinimal.h"
#include "STU_Ammo_Decorator.generated.h"

class ASTU_Base_Weapon;

UCLASS()
class SHOOT_THEM_UP_API USTU_Ammo_Decorator : public UBTDecorator
{
    GENERATED_BODY()

public:
    USTU_Ammo_Decorator();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    TSubclassOf<ASTU_Base_Weapon> weapon_type;

    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp,
                                            uint8*                  NodeMemory) const override;
};
