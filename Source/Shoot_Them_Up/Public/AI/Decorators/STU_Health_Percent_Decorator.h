// Shoot_Them_Up game from Ckopnuk

#pragma once

#include "BehaviorTree/BTDecorator.h"
#include "CoreMinimal.h"

#include "STU_Health_Percent_Decorator.generated.h"

UCLASS()
class SHOOT_THEM_UP_API USTU_Health_Percent_Decorator : public UBTDecorator
{

    GENERATED_BODY()

public:
    USTU_Health_Percent_Decorator();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    float health_percent_to_hill = 0.6;

    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp,
                                            uint8*                  NodeMemory) const override;
};
