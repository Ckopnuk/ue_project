// Shoot_Them_Up game from Ckopnuk

using UnrealBuildTool;
using System.Collections.Generic;

public class Shoot_Them_UpEditorTarget : TargetRules
{
	public Shoot_Them_UpEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Shoot_Them_Up" } );
	}
}
