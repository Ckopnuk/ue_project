// Shoot_Them_Up game from Ckopnuk

using UnrealBuildTool;
using System.Collections.Generic;

public class Shoot_Them_UpTarget : TargetRules
{
	public Shoot_Them_UpTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Shoot_Them_Up" } );
	}
}
